//[core_shared](index.md)

# core_shared

## Packages

| Name |
|---|
| [multi.platform.core.shared](core_shared/multi.platform.core.shared/index.md) |
| [multi.platform.core.shared.app.common](core_shared/multi.platform.core.shared.app.common/index.md) |
| [multi.platform.core.shared.app.errorconnectiondialog](core_shared/multi.platform.core.shared.app.errorconnectiondialog/index.md) |
| [multi.platform.core.shared.data.common.network.response](core_shared/multi.platform.core.shared.data.common.network.response/index.md) |
| [multi.platform.core.shared.domain.common.usecase](core_shared/multi.platform.core.shared.domain.common.usecase/index.md) |
| [multi.platform.core.shared.external](core_shared/multi.platform.core.shared.external/index.md) |
| [multi.platform.core.shared.external.constants](core_shared/multi.platform.core.shared.external.constants/index.md) |
| [multi.platform.core.shared.external.enums](core_shared/multi.platform.core.shared.external.enums/index.md) |
| [multi.platform.core.shared.external.extensions](core_shared/multi.platform.core.shared.external.extensions/index.md) |
| [multi.platform.core.shared.external.utilities](core_shared/multi.platform.core.shared.external.utilities/index.md) |
| [multi.platform.core.shared.external.utilities.network](core_shared/multi.platform.core.shared.external.utilities.network/index.md) |
