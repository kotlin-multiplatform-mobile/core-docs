//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities](../index.md)/[Validation](index.md)/[notBlank](not-blank.md)

# notBlank

[common]\
fun [notBlank](not-blank.md)(data: [CharSequence](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-char-sequence/index.html)?): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?
