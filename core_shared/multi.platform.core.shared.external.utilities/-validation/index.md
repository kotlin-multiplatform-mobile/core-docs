//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities](../index.md)/[Validation](index.md)

# Validation

[common]\
object [Validation](index.md)

## Functions

| Name | Summary |
|---|---|
| [emailFormat](email-format.md) | [common]<br>fun [emailFormat](email-format.md)(data: [CharSequence](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-char-sequence/index.html)?): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)? |
| [minCharacter](min-character.md) | [common]<br>fun [minCharacter](min-character.md)(minCharacter: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), data: [CharSequence](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-char-sequence/index.html)?): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)? |
| [notBlank](not-blank.md) | [common]<br>fun [notBlank](not-blank.md)(data: [CharSequence](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-char-sequence/index.html)?): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)? |
| [passwordFormat](password-format.md) | [common]<br>fun [passwordFormat](password-format.md)(data: [CharSequence](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-char-sequence/index.html)?): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)? |
| [phoneFormat](phone-format.md) | [common]<br>fun [phoneFormat](phone-format.md)(data: [CharSequence](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-char-sequence/index.html)?): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)? |
