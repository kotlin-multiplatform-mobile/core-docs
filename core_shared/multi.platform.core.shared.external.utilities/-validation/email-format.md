//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities](../index.md)/[Validation](index.md)/[emailFormat](email-format.md)

# emailFormat

[common]\
fun [emailFormat](email-format.md)(data: [CharSequence](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-char-sequence/index.html)?): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?
