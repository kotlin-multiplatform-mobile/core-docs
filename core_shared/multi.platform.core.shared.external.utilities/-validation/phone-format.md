//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities](../index.md)/[Validation](index.md)/[phoneFormat](phone-format.md)

# phoneFormat

[common]\
fun [phoneFormat](phone-format.md)(data: [CharSequence](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-char-sequence/index.html)?): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?
