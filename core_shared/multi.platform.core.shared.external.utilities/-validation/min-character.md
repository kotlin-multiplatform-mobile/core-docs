//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities](../index.md)/[Validation](index.md)/[minCharacter](min-character.md)

# minCharacter

[common]\
fun [minCharacter](min-character.md)(minCharacter: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), data: [CharSequence](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-char-sequence/index.html)?): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?
