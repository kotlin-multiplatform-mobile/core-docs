//[core_shared](../../index.md)/[multi.platform.core.shared.external.utilities](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [LocaleUtil](-locale-util/index.md) | [android]<br>object [LocaleUtil](-locale-util/index.md) |
| [SharedPrefsUtil](-shared-prefs-util/index.md) | [android]<br>object [SharedPrefsUtil](-shared-prefs-util/index.md) |
| [Validation](-validation/index.md) | [common]<br>object [Validation](-validation/index.md) |
