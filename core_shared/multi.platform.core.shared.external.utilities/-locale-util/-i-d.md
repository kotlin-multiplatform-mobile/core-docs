//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities](../index.md)/[LocaleUtil](index.md)/[ID](-i-d.md)

# ID

[android]\
const val [ID](-i-d.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
