//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities](../index.md)/[LocaleUtil](index.md)/[setLocale](set-locale.md)

# setLocale

[android]\
fun [setLocale](set-locale.md)(context: [Context](https://developer.android.com/reference/kotlin/android/content/Context.html), language: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [Context](https://developer.android.com/reference/kotlin/android/content/Context.html)
