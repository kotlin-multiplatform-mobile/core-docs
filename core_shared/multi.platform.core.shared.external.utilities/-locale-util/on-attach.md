//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities](../index.md)/[LocaleUtil](index.md)/[onAttach](on-attach.md)

# onAttach

[android]\
fun [onAttach](on-attach.md)(c: [Context](https://developer.android.com/reference/kotlin/android/content/Context.html)?, l: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = ID): [Context](https://developer.android.com/reference/kotlin/android/content/Context.html)?
