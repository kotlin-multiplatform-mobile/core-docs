//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities](../index.md)/[LocaleUtil](index.md)

# LocaleUtil

[android]\
object [LocaleUtil](index.md)

## Properties

| Name | Summary |
|---|---|
| [AUTO](-a-u-t-o.md) | [android]<br>const val [AUTO](-a-u-t-o.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [EN](-e-n.md) | [android]<br>const val [EN](-e-n.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [ID](-i-d.md) | [android]<br>const val [ID](-i-d.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |

## Functions

| Name | Summary |
|---|---|
| [onAttach](on-attach.md) | [android]<br>fun [onAttach](on-attach.md)(c: [Context](https://developer.android.com/reference/kotlin/android/content/Context.html)?, l: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = ID): [Context](https://developer.android.com/reference/kotlin/android/content/Context.html)? |
| [retrieveAppLanguage](retrieve-app-language.md) | [android]<br>fun [retrieveAppLanguage](retrieve-app-language.md)(context: [Context](https://developer.android.com/reference/kotlin/android/content/Context.html), defaultLanguage: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = ID): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [setLocale](set-locale.md) | [android]<br>fun [setLocale](set-locale.md)(context: [Context](https://developer.android.com/reference/kotlin/android/content/Context.html), language: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [Context](https://developer.android.com/reference/kotlin/android/content/Context.html) |
