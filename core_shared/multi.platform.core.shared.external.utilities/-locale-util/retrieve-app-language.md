//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities](../index.md)/[LocaleUtil](index.md)/[retrieveAppLanguage](retrieve-app-language.md)

# retrieveAppLanguage

[android]\
fun [retrieveAppLanguage](retrieve-app-language.md)(context: [Context](https://developer.android.com/reference/kotlin/android/content/Context.html), defaultLanguage: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = ID): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
