//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities](../index.md)/[SharedPrefsUtil](index.md)

# SharedPrefsUtil

[android]\
object [SharedPrefsUtil](index.md)

## Functions

| Name | Summary |
|---|---|
| [build](build.md) | [android]<br>fun [build](build.md)(context: [Context](https://developer.android.com/reference/kotlin/android/content/Context.html), name: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [SharedPreferences](https://developer.android.com/reference/kotlin/android/content/SharedPreferences.html) |
