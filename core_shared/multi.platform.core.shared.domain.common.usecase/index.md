//[core_shared](../../index.md)/[multi.platform.core.shared.domain.common.usecase](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [CoreUseCase](-core-use-case/index.md) | [common]<br>interface [CoreUseCase](-core-use-case/index.md) |
| [RefreshTokenUseCase](-refresh-token-use-case/index.md) | [common]<br>interface [RefreshTokenUseCase](-refresh-token-use-case/index.md) : [CoreUseCase](-core-use-case/index.md) |
