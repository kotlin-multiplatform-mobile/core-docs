//[core_shared](../../../index.md)/[multi.platform.core.shared.domain.common.usecase](../index.md)/[RefreshTokenUseCase](index.md)/[call](call.md)

# call

[common]\
abstract suspend override fun [call](call.md)(vararg args: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)?)

Override this function for get new token and refresh token. Don't forget to store response value to newToken and newRefreshToken variable. With stored response to mentioned variable, will be saved to encrypted local data. Example: Invoke repository that handle refresh token api
