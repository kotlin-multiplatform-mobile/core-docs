//[core_shared](../../../index.md)/[multi.platform.core.shared.domain.common.usecase](../index.md)/[RefreshTokenUseCase](index.md)/[onError](on-error.md)

# onError

[common]\
abstract suspend override fun [onError](on-error.md)(e: [Exception](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-exception/index.html))

Override this function for handling any error occurred. Example: Remove saved token and refresh token
