//[core_shared](../../../index.md)/[multi.platform.core.shared.domain.common.usecase](../index.md)/[RefreshTokenUseCase](index.md)/[newToken](new-token.md)

# newToken

[common]\
abstract var [newToken](new-token.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

This variable should valued from succeed call(). Useful for saving to local data and retry current expired api call
