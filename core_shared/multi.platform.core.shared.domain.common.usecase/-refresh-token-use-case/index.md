//[core_shared](../../../index.md)/[multi.platform.core.shared.domain.common.usecase](../index.md)/[RefreshTokenUseCase](index.md)

# RefreshTokenUseCase

[common]\
interface [RefreshTokenUseCase](index.md) : [CoreUseCase](../-core-use-case/index.md)

## Properties

| Name | Summary |
|---|---|
| [newRefreshToken](new-refresh-token.md) | [common]<br>abstract var [newRefreshToken](new-refresh-token.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)<br>This variable should valued from succeed call(). Useful for saving to local data |
| [newToken](new-token.md) | [common]<br>abstract var [newToken](new-token.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)<br>This variable should valued from succeed call(). Useful for saving to local data and retry current expired api call |

## Functions

| Name | Summary |
|---|---|
| [call](call.md) | [common]<br>abstract suspend override fun [call](call.md)(vararg args: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)?)<br>Override this function for get new token and refresh token. Don't forget to store response value to newToken and newRefreshToken variable. With stored response to mentioned variable, will be saved to encrypted local data. Example: Invoke repository that handle refresh token api |
| [onError](on-error.md) | [common]<br>abstract suspend override fun [onError](on-error.md)(e: [Exception](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-exception/index.html))<br>Override this function for handling any error occurred. Example: Remove saved token and refresh token |
