//[core_shared](../../../index.md)/[multi.platform.core.shared.domain.common.usecase](../index.md)/[CoreUseCase](index.md)

# CoreUseCase

interface [CoreUseCase](index.md)

#### Inheritors

| |
|---|
| [RefreshTokenUseCase](../-refresh-token-use-case/index.md) |

## Functions

| Name | Summary |
|---|---|
| [call](call.md) | [common]<br>abstract suspend fun [call](call.md)(vararg args: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)?): [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html) |
| [onError](on-error.md) | [common]<br>abstract suspend fun [onError](on-error.md)(e: [Exception](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-exception/index.html)) |
