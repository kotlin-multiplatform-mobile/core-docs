//[core_shared](../../../index.md)/[multi.platform.core.shared.domain.common.usecase](../index.md)/[CoreUseCase](index.md)/[onError](on-error.md)

# onError

[common]\
abstract suspend fun [onError](on-error.md)(e: [Exception](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-exception/index.html))
