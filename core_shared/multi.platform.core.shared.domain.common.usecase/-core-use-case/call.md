//[core_shared](../../../index.md)/[multi.platform.core.shared.domain.common.usecase](../index.md)/[CoreUseCase](index.md)/[call](call.md)

# call

[common]\
abstract suspend fun [call](call.md)(vararg args: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)?): [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)
