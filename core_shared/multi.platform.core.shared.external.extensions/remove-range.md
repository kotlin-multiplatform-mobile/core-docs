//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[removeRange](remove-range.md)

# removeRange

[common]\
inline fun &lt;[T](remove-range.md)&gt; [MutableList](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-mutable-list/index.html)&lt;[T](remove-range.md)&gt;.[removeRange](remove-range.md)(range: [IntRange](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.ranges/-int-range/index.html))
