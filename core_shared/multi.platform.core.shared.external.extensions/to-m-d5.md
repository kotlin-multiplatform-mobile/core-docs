//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[toMD5](to-m-d5.md)

# toMD5

[android]\
fun [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html).[toMD5](to-m-d5.md)(): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
