//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[getPathFromURI](get-path-from-u-r-i.md)

# getPathFromURI

[android]\
fun [Fragment](https://developer.android.com/reference/kotlin/androidx/fragment/app/Fragment.html).[getPathFromURI](get-path-from-u-r-i.md)(contentUri: [Uri](https://developer.android.com/reference/kotlin/android/net/Uri.html)?): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?

Extension for get path file from URI
