//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[roundAsString](round-as-string.md)

# roundAsString

[android]\
fun [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html).[roundAsString](round-as-string.md)(maxFractionDigits: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = 0): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
