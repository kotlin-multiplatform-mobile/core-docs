//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[showErrorSnackbar](show-error-snackbar.md)

# showErrorSnackbar

[android]\
fun [Fragment](https://developer.android.com/reference/kotlin/androidx/fragment/app/Fragment.html).[showErrorSnackbar](show-error-snackbar.md)(anchor: [View](https://developer.android.com/reference/kotlin/android/view/View.html), message: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, duration: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = Snackbar.LENGTH_SHORT)

Extension for show error snack bar
