//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[showKeyboard](show-keyboard.md)

# showKeyboard

[android]\
fun [Fragment](https://developer.android.com/reference/kotlin/androidx/fragment/app/Fragment.html).[showKeyboard](show-keyboard.md)(focusView: [View](https://developer.android.com/reference/kotlin/android/view/View.html))

Extension for show soft keyboard
