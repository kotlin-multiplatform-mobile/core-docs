//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[showToast](show-toast.md)

# showToast

[android]\
fun [Fragment](https://developer.android.com/reference/kotlin/androidx/fragment/app/Fragment.html).[showToast](show-toast.md)(message: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, duration: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = Toast.LENGTH_SHORT)

Extension for show toast message
