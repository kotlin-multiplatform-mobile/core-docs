//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[hideKeyboard](hide-keyboard.md)

# hideKeyboard

[android]\
fun [Fragment](https://developer.android.com/reference/kotlin/androidx/fragment/app/Fragment.html).[hideKeyboard](hide-keyboard.md)()

Extension for hide soft keyboard
