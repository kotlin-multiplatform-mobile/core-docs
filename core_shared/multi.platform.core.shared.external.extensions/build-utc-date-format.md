//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[buildUtcDateFormat](build-utc-date-format.md)

# buildUtcDateFormat

[android]\
fun [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html).[buildUtcDateFormat](build-utc-date-format.md)(): [SimpleDateFormat](https://developer.android.com/reference/kotlin/java/text/SimpleDateFormat.html)
