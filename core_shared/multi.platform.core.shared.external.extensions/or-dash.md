//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[orDash](or-dash.md)

# orDash

[android]\
fun [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?.[orDash](or-dash.md)(): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
