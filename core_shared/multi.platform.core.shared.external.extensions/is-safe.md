//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[isSafe](is-safe.md)

# isSafe

[android]\
fun [Fragment](https://developer.android.com/reference/kotlin/androidx/fragment/app/Fragment.html).[isSafe](is-safe.md)(): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

Extension for safely modify ui
