//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[isInBackStack](is-in-back-stack.md)

# isInBackStack

[android]\
fun [Fragment](https://developer.android.com/reference/kotlin/androidx/fragment/app/Fragment.html).[isInBackStack](is-in-back-stack.md)(destinationId: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

fun [NavController](https://developer.android.com/reference/kotlin/androidx/navigation/NavController.html).[isInBackStack](is-in-back-stack.md)(destinationId: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

Extension for check route id in back stack
