//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[loadImage](load-image.md)

# loadImage

[android]\
fun [ImageView](https://developer.android.com/reference/kotlin/android/widget/ImageView.html).[loadImage](load-image.md)(any: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html), options: RequestOptions = RequestOptions())

fun [ImageView](https://developer.android.com/reference/kotlin/android/widget/ImageView.html).[loadImage](load-image.md)(any: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html), errorPlaceholder: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)?, options: RequestOptions = RequestOptions())

fun [ImageView](https://developer.android.com/reference/kotlin/android/widget/ImageView.html).[loadImage](load-image.md)(any: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html), loadingPlaceholder: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)?, errorPlaceholder: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)?, options: RequestOptions = RequestOptions())
