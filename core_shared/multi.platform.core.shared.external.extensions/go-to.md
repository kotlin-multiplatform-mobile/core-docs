//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[goTo](go-to.md)

# goTo

[android]\
fun [Fragment](https://developer.android.com/reference/kotlin/androidx/fragment/app/Fragment.html).[goTo](go-to.md)(path: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), navOptions: [NavOptions](https://developer.android.com/reference/kotlin/androidx/navigation/NavOptions.html)? = null)

Extension for direction to fragment path
