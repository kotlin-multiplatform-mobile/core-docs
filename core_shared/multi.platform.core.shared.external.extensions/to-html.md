//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[toHtml](to-html.md)

# toHtml

[android]\
fun [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html).[toHtml](to-html.md)(): [Spanned](https://developer.android.com/reference/kotlin/android/text/Spanned.html)?
