//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[blockingClickListener](blocking-click-listener.md)

# blockingClickListener

[android]\
fun [View](https://developer.android.com/reference/kotlin/android/view/View.html).[blockingClickListener](blocking-click-listener.md)(debounceTime: [Long](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html) = 1200, action: () -&gt; [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html))
