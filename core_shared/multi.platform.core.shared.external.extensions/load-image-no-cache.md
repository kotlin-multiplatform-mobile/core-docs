//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[loadImageNoCache](load-image-no-cache.md)

# loadImageNoCache

[android]\
fun [ImageView](https://developer.android.com/reference/kotlin/android/widget/ImageView.html).[loadImageNoCache](load-image-no-cache.md)(any: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html), options: RequestOptions = RequestOptions())

fun [ImageView](https://developer.android.com/reference/kotlin/android/widget/ImageView.html).[loadImageNoCache](load-image-no-cache.md)(any: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html), errorPlaceholder: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)?, options: RequestOptions = RequestOptions())

fun [ImageView](https://developer.android.com/reference/kotlin/android/widget/ImageView.html).[loadImageNoCache](load-image-no-cache.md)(any: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html), loadingPlaceholder: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)?, errorPlaceholder: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)?, options: RequestOptions = RequestOptions())
