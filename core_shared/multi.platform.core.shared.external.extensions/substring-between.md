//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[substringBetween](substring-between.md)

# substringBetween

[android]\
fun [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html).[substringBetween](substring-between.md)(startDelimiter: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), endDelimiter: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
