//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[buildSimpleDateFormat](build-simple-date-format.md)

# buildSimpleDateFormat

[android]\
fun [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html).[buildSimpleDateFormat](build-simple-date-format.md)(): [SimpleDateFormat](https://developer.android.com/reference/kotlin/java/text/SimpleDateFormat.html)
