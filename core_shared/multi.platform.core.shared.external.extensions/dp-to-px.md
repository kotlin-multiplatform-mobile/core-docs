//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[dpToPx](dp-to-px.md)

# dpToPx

[android]\
fun [Fragment](https://developer.android.com/reference/kotlin/androidx/fragment/app/Fragment.html).[dpToPx](dp-to-px.md)(dp: [Float](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-float/index.html)): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)

Extension for show converting dp to px
