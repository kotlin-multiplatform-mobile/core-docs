//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[launchAndCollectIn](launch-and-collect-in.md)

# launchAndCollectIn

[android]\
inline fun &lt;[T](launch-and-collect-in.md)&gt; Flow&lt;[T](launch-and-collect-in.md)&gt;.[launchAndCollectIn](launch-and-collect-in.md)(owner: [LifecycleOwner](https://developer.android.com/reference/kotlin/androidx/lifecycle/LifecycleOwner.html), minActiveState: [Lifecycle.State](https://developer.android.com/reference/kotlin/androidx/lifecycle/Lifecycle.State.html) = Lifecycle.State.STARTED, crossinline action: suspend CoroutineScope.([T](launch-and-collect-in.md)) -&gt; [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)): Job
