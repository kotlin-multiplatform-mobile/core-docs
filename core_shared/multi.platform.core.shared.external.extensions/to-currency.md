//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[toCurrency](to-currency.md)

# toCurrency

[android]\
fun [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html).[toCurrency](to-currency.md)(symbol: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), maxFractionDigits: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = 0): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

fun [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html).[toCurrency](to-currency.md)(symbol: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
