//[core_shared](../../index.md)/[multi.platform.core.shared.external.extensions](index.md)/[getAlphaNumericDashAfterColon](get-alpha-numeric-dash-after-colon.md)

# getAlphaNumericDashAfterColon

[android]\
fun [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html).[getAlphaNumericDashAfterColon](get-alpha-numeric-dash-after-colon.md)(): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
