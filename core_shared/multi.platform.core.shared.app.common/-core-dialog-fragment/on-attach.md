//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreDialogFragment](index.md)/[onAttach](on-attach.md)

# onAttach

[android]\
open override fun [onAttach](on-attach.md)(context: [Context](https://developer.android.com/reference/kotlin/android/content/Context.html))
