//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewModel](index.md)/[validatePasswordFormat](validate-password-format.md)

# validatePasswordFormat

[common, android, ios]\
[common]\
expect fun [validatePasswordFormat](validate-password-format.md)(field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

[android, ios]\
actual fun [validatePasswordFormat](validate-password-format.md)(field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)
