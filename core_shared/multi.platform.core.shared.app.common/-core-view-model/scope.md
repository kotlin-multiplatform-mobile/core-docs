//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewModel](index.md)/[scope](scope.md)

# scope

[common, android, ios]\
[common]\
expect val [scope](scope.md): CoroutineScope

[android, ios]\
actual val [scope](scope.md): CoroutineScope
