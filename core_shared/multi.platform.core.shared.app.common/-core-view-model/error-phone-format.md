//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewModel](index.md)/[errorPhoneFormat](error-phone-format.md)

# errorPhoneFormat

[common, android, ios]\
[common]\
expect var [errorPhoneFormat](error-phone-format.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?

[android, ios]\
actual var [errorPhoneFormat](error-phone-format.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
