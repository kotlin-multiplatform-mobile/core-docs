//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewModel](index.md)/[isFromNetwork](is-from-network.md)

# isFromNetwork

[common, android, ios]\
[common]\
expect var [isFromNetwork](is-from-network.md): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

[android, ios]\
actual var [isFromNetwork](is-from-network.md): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)
