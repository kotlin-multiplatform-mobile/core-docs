//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewModel](index.md)/[errorPasswordFormat](error-password-format.md)

# errorPasswordFormat

[common, android, ios]\
[common]\
expect var [errorPasswordFormat](error-password-format.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?

[android, ios]\
actual var [errorPasswordFormat](error-password-format.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
