//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewModel](index.md)/[validateMinChar](validate-min-char.md)

# validateMinChar

[common, android, ios]\
[common]\
expect fun [validateMinChar](validate-min-char.md)(min: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

[android, ios]\
actual fun [validateMinChar](validate-min-char.md)(min: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)
