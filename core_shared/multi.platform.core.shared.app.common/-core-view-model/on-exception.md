//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewModel](index.md)/[onException](on-exception.md)

# onException

[common, android, ios]\
[common]\
expect val [onException](on-exception.md): MutableStateFlow&lt;[Exception](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-exception/index.html)?&gt;

[android, ios]\
actual val [onException](on-exception.md): MutableStateFlow&lt;[Exception](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-exception/index.html)?&gt;
