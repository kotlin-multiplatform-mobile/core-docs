//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewModel](index.md)/[loadingIndicator](loading-indicator.md)

# loadingIndicator

[common, android, ios]\
[common]\
expect val [loadingIndicator](loading-indicator.md): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?&gt;

[android, ios]\
actual val [loadingIndicator](loading-indicator.md): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?&gt;
