//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewModel](index.md)/[validateBlank](validate-blank.md)

# validateBlank

[common, android, ios]\
[common]\
expect fun [validateBlank](validate-blank.md)(field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

[android, ios]\
actual fun [validateBlank](validate-blank.md)(field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)
