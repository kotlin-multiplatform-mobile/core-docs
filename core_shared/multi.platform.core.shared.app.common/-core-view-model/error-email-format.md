//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewModel](index.md)/[errorEmailFormat](error-email-format.md)

# errorEmailFormat

[common, android, ios]\
[common]\
expect var [errorEmailFormat](error-email-format.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?

[android, ios]\
actual var [errorEmailFormat](error-email-format.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
