//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewModel](index.md)/[errorResponse](error-response.md)

# errorResponse

[common, android, ios]\
[common]\
expect val [errorResponse](error-response.md): MutableStateFlow&lt;HttpResponse?&gt;

[android, ios]\
actual val [errorResponse](error-response.md): MutableStateFlow&lt;HttpResponse?&gt;
