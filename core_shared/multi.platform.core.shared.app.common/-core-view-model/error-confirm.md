//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewModel](index.md)/[errorConfirm](error-confirm.md)

# errorConfirm

[common, android, ios]\
[common]\
expect var [errorConfirm](error-confirm.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?

[android, ios]\
actual var [errorConfirm](error-confirm.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
