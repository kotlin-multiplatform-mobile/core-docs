//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewModel](index.md)/[validateConfirm](validate-confirm.md)

# validateConfirm

[common, android, ios]\
[common]\
expect fun [validateConfirm](validate-confirm.md)(field1: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, field2: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

[android, ios]\
actual fun [validateConfirm](validate-confirm.md)(field1: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, field2: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)
