//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewModel](index.md)/[useAsyncNetworkCall](use-async-network-call.md)

# useAsyncNetworkCall

[common, android, ios]\
[common]\
expect var [useAsyncNetworkCall](use-async-network-call.md): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

[android, ios]\
actual var [useAsyncNetworkCall](use-async-network-call.md): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)
