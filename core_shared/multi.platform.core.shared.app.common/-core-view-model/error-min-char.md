//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewModel](index.md)/[errorMinChar](error-min-char.md)

# errorMinChar

[common, android, ios]\
[common]\
expect var [errorMinChar](error-min-char.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?

[android, ios]\
actual var [errorMinChar](error-min-char.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
