//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewModel](index.md)/[isEmpty](is-empty.md)

# isEmpty

[common, android, ios]\
[common]\
expect val [isEmpty](is-empty.md): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)&gt;

[android, ios]\
actual val [isEmpty](is-empty.md): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)&gt;
