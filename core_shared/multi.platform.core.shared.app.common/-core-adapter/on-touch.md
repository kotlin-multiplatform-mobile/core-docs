//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreAdapter](index.md)/[onTouch](on-touch.md)

# onTouch

[android]\
var [onTouch](on-touch.md): ([View](https://developer.android.com/reference/kotlin/android/view/View.html), [MotionEvent](https://developer.android.com/reference/kotlin/android/view/MotionEvent.html)) -&gt; [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?
