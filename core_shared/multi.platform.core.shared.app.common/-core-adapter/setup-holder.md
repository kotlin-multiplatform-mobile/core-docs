//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreAdapter](index.md)/[setupHolder](setup-holder.md)

# setupHolder

[android]\
abstract fun [setupHolder](setup-holder.md)(parent: [ViewGroup](https://developer.android.com/reference/kotlin/android/view/ViewGroup.html), viewType: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)): [CoreViewHolder](../-core-view-holder/index.md)&lt;[D](index.md)&gt;
