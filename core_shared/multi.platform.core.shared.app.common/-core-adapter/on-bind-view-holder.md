//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreAdapter](index.md)/[onBindViewHolder](on-bind-view-holder.md)

# onBindViewHolder

[android]\
open override fun [onBindViewHolder](on-bind-view-holder.md)(holder: [CoreViewHolder](../-core-view-holder/index.md)&lt;[D](index.md)&gt;, position: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html))
