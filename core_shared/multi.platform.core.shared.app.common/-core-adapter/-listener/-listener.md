//[core_shared](../../../../index.md)/[multi.platform.core.shared.app.common](../../index.md)/[CoreAdapter](../index.md)/[Listener](index.md)/[Listener](-listener.md)

# Listener

[android]\
constructor(swipeRefreshLayout: [SwipeRefreshLayout](https://developer.android.com/reference/kotlin/androidx/swiperefreshlayout/widget/SwipeRefreshLayout.html)? = null, fetchArg: () -&gt; [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)? = null)
