//[core_shared](../../../../index.md)/[multi.platform.core.shared.app.common](../../index.md)/[CoreAdapter](../index.md)/[Listener](index.md)

# Listener

[android]\
inner class [Listener](index.md)(swipeRefreshLayout: [SwipeRefreshLayout](https://developer.android.com/reference/kotlin/androidx/swiperefreshlayout/widget/SwipeRefreshLayout.html)? = null, fetchArg: () -&gt; [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)? = null) : [RecyclerView.OnScrollListener](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.OnScrollListener.html)

## Constructors

| | |
|---|---|
| [Listener](-listener.md) | [android]<br>constructor(swipeRefreshLayout: [SwipeRefreshLayout](https://developer.android.com/reference/kotlin/androidx/swiperefreshlayout/widget/SwipeRefreshLayout.html)? = null, fetchArg: () -&gt; [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)? = null) |

## Functions

| Name | Summary |
|---|---|
| [onScrolled](on-scrolled.md) | [android]<br>open override fun [onScrolled](on-scrolled.md)(recyclerView: [RecyclerView](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.html), dx: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), dy: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)) |
| [onScrollStateChanged](index.md#-767679993%2FFunctions%2F-2121679934) | [android]<br>open fun [onScrollStateChanged](index.md#-767679993%2FFunctions%2F-2121679934)(@[NonNull](https://developer.android.com/reference/kotlin/androidx/annotation/NonNull.html)p0: [RecyclerView](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.html), p1: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)) |
