//[core_shared](../../../../index.md)/[multi.platform.core.shared.app.common](../../index.md)/[CoreAdapter](../index.md)/[Listener](index.md)/[onScrolled](on-scrolled.md)

# onScrolled

[android]\
open override fun [onScrolled](on-scrolled.md)(recyclerView: [RecyclerView](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.html), dx: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), dy: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html))
