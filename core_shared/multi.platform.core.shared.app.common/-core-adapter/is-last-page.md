//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreAdapter](index.md)/[isLastPage](is-last-page.md)

# isLastPage

[android]\
var [isLastPage](is-last-page.md): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)
