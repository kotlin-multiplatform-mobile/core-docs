//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreAdapter](index.md)/[showSkeletons](show-skeletons.md)

# showSkeletons

[android]\
fun [showSkeletons](show-skeletons.md)(size: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = fetchLimit)
