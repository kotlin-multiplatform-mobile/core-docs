//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreAdapter](index.md)/[height](height.md)

# height

[android]\
var [height](height.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)
