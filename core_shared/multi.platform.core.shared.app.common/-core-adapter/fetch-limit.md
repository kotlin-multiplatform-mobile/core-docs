//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreAdapter](index.md)/[fetchLimit](fetch-limit.md)

# fetchLimit

[android]\
var [fetchLimit](fetch-limit.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)
