//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreAdapter](index.md)/[itemsCache](items-cache.md)

# itemsCache

[android]\
var [itemsCache](items-cache.md): [MutableList](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-mutable-list/index.html)&lt;[D](index.md)&gt;
