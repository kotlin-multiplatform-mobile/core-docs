//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreAdapter](index.md)/[elevation](elevation.md)

# elevation

[android]\
var [elevation](elevation.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)
