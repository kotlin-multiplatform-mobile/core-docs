//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreAdapter](index.md)/[onCreateViewHolder](on-create-view-holder.md)

# onCreateViewHolder

[android]\
open override fun [onCreateViewHolder](on-create-view-holder.md)(parent: [ViewGroup](https://developer.android.com/reference/kotlin/android/view/ViewGroup.html), viewType: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)): [CoreViewHolder](../-core-view-holder/index.md)&lt;[D](index.md)&gt;
