//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreAdapter](index.md)/[onSelected](on-selected.md)

# onSelected

[android]\
var [onSelected](on-selected.md): ([View](https://developer.android.com/reference/kotlin/android/view/View.html), [D](index.md)) -&gt; [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)?
