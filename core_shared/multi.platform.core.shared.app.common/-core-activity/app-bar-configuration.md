//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreActivity](index.md)/[appBarConfiguration](app-bar-configuration.md)

# appBarConfiguration

[android]\
lateinit var [appBarConfiguration](app-bar-configuration.md): [AppBarConfiguration](https://developer.android.com/reference/kotlin/androidx/navigation/ui/AppBarConfiguration.html)
