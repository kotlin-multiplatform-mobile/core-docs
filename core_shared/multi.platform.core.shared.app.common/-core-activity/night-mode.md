//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreActivity](index.md)/[nightMode](night-mode.md)

# nightMode

[android]\
open fun [nightMode](night-mode.md)(): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)

Open function for override night mode Default: MODE_NIGHT_NO
