//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreActivity](index.md)/[orientation](orientation.md)

# orientation

[android]\
open fun [orientation](orientation.md)(): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)

Open function for application orientation Default: ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
