//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreActivity](index.md)/[isInternetAvailable](is-internet-available.md)

# isInternetAvailable

[android]\
fun [isInternetAvailable](is-internet-available.md)(): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

Function for check internet availability

#### Return

boolean
