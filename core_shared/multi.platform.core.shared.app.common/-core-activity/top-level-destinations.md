//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreActivity](index.md)/[topLevelDestinations](top-level-destinations.md)

# topLevelDestinations

[android]\
open fun [topLevelDestinations](top-level-destinations.md)(): [Set](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-set/index.html)&lt;[Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)&gt;

Open function for override default top level app route config Default: no top route
