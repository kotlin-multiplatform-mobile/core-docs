//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreSheetFragment](index.md)/[onCreateDialog](on-create-dialog.md)

# onCreateDialog

[android]\
open override fun [onCreateDialog](on-create-dialog.md)(savedInstanceState: [Bundle](https://developer.android.com/reference/kotlin/android/os/Bundle.html)?): [Dialog](https://developer.android.com/reference/kotlin/android/app/Dialog.html)
