//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewHolder](index.md)/[cardView](card-view.md)

# cardView

[android]\
open val [cardView](card-view.md): [CardView](https://developer.android.com/reference/kotlin/androidx/cardview/widget/CardView.html)? = null
