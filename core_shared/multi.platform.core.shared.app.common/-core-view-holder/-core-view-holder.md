//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewHolder](index.md)/[CoreViewHolder](-core-view-holder.md)

# CoreViewHolder

[android]\
constructor(root: [View](https://developer.android.com/reference/kotlin/android/view/View.html), radius: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = 0, elevation: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = 0, onSelected: ([View](https://developer.android.com/reference/kotlin/android/view/View.html), [D](index.md)) -&gt; [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)? = null, onTouch: ([View](https://developer.android.com/reference/kotlin/android/view/View.html), [MotionEvent](https://developer.android.com/reference/kotlin/android/view/MotionEvent.html)) -&gt; [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)? = null)
