//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewHolder](index.md)/[shine](shine.md)

# shine

[android]\
open val [shine](shine.md): [View](https://developer.android.com/reference/kotlin/android/view/View.html)? = null
