//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewHolder](index.md)

# CoreViewHolder

[android]\
abstract class [CoreViewHolder](index.md)&lt;[D](index.md)&gt;(val root: [View](https://developer.android.com/reference/kotlin/android/view/View.html), val radius: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = 0, elevation: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = 0, onSelected: ([View](https://developer.android.com/reference/kotlin/android/view/View.html), [D](index.md)) -&gt; [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)? = null, onTouch: ([View](https://developer.android.com/reference/kotlin/android/view/View.html), [MotionEvent](https://developer.android.com/reference/kotlin/android/view/MotionEvent.html)) -&gt; [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)? = null) : [RecyclerView.ViewHolder](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.ViewHolder.html)

## Constructors

| | |
|---|---|
| [CoreViewHolder](-core-view-holder.md) | [android]<br>constructor(root: [View](https://developer.android.com/reference/kotlin/android/view/View.html), radius: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = 0, elevation: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = 0, onSelected: ([View](https://developer.android.com/reference/kotlin/android/view/View.html), [D](index.md)) -&gt; [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)? = null, onTouch: ([View](https://developer.android.com/reference/kotlin/android/view/View.html), [MotionEvent](https://developer.android.com/reference/kotlin/android/view/MotionEvent.html)) -&gt; [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)? = null) |

## Properties

| Name | Summary |
|---|---|
| [cardView](card-view.md) | [android]<br>open val [cardView](card-view.md): [CardView](https://developer.android.com/reference/kotlin/androidx/cardview/widget/CardView.html)? = null |
| [itemView](index.md#29975211%2FProperties%2F-2121679934) | [android]<br>@[NonNull](https://developer.android.com/reference/kotlin/androidx/annotation/NonNull.html)<br>val [itemView](index.md#29975211%2FProperties%2F-2121679934): [View](https://developer.android.com/reference/kotlin/android/view/View.html) |
| [radius](radius.md) | [android]<br>val [radius](radius.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = 0 |
| [root](root.md) | [android]<br>val [root](root.md): [View](https://developer.android.com/reference/kotlin/android/view/View.html) |
| [shine](shine.md) | [android]<br>open val [shine](shine.md): [View](https://developer.android.com/reference/kotlin/android/view/View.html)? = null |

## Functions

| Name | Summary |
|---|---|
| [bind](bind.md) | [android]<br>open fun [bind](bind.md)(item: [D](index.md), state: [StateEnum](../../multi.platform.core.shared.external.enums/-state-enum/index.md)? = null) |
| [getAdapterPosition](index.md#644519777%2FFunctions%2F-2121679934) | [android]<br>fun [getAdapterPosition](index.md#644519777%2FFunctions%2F-2121679934)(): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [getItemId](index.md#1378485811%2FFunctions%2F-2121679934) | [android]<br>fun [getItemId](index.md#1378485811%2FFunctions%2F-2121679934)(): [Long](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html) |
| [getItemViewType](index.md#-1649344625%2FFunctions%2F-2121679934) | [android]<br>fun [getItemViewType](index.md#-1649344625%2FFunctions%2F-2121679934)(): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [getLayoutPosition](index.md#-1407255826%2FFunctions%2F-2121679934) | [android]<br>fun [getLayoutPosition](index.md#-1407255826%2FFunctions%2F-2121679934)(): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [getOldPosition](index.md#-1203059319%2FFunctions%2F-2121679934) | [android]<br>fun [getOldPosition](index.md#-1203059319%2FFunctions%2F-2121679934)(): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [getPosition](index.md#-1155470344%2FFunctions%2F-2121679934) | [android]<br>fun [~~getPosition~~](index.md#-1155470344%2FFunctions%2F-2121679934)(): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [isRecyclable](index.md#-1703443315%2FFunctions%2F-2121679934) | [android]<br>fun [isRecyclable](index.md#-1703443315%2FFunctions%2F-2121679934)(): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [onEmpty](on-empty.md) | [android]<br>open fun [onEmpty](on-empty.md)() |
| [onError](on-error.md) | [android]<br>open fun [onError](on-error.md)() |
| [onLoading](on-loading.md) | [android]<br>open fun [onLoading](on-loading.md)() |
| [onSuccess](on-success.md) | [android]<br>open fun [onSuccess](on-success.md)(item: [D](index.md)) |
| [setIsRecyclable](index.md#-1860912636%2FFunctions%2F-2121679934) | [android]<br>fun [setIsRecyclable](index.md#-1860912636%2FFunctions%2F-2121679934)(p0: [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)) |
| [toString](index.md#-1200015593%2FFunctions%2F-2121679934) | [android]<br>open override fun [toString](index.md#-1200015593%2FFunctions%2F-2121679934)(): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
