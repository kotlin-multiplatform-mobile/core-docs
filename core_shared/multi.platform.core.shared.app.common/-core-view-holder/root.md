//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewHolder](index.md)/[root](root.md)

# root

[android]\
val [root](root.md): [View](https://developer.android.com/reference/kotlin/android/view/View.html)
