//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewHolder](index.md)/[bind](bind.md)

# bind

[android]\
open fun [bind](bind.md)(item: [D](index.md), state: [StateEnum](../../multi.platform.core.shared.external.enums/-state-enum/index.md)? = null)
