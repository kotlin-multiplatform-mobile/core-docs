//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[CoreViewHolder](index.md)/[radius](radius.md)

# radius

[android]\
val [radius](radius.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = 0
