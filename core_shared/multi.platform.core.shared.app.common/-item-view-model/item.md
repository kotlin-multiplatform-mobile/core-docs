//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[ItemViewModel](index.md)/[item](item.md)

# item

[common]\
var [item](item.md): MutableStateFlow&lt;[ITEM](index.md)?&gt;
