//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[ItemViewModel](index.md)/[getFromNetwork](get-from-network.md)

# getFromNetwork

[common]\
open fun [getFromNetwork](get-from-network.md)(idArg: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)? = null)
