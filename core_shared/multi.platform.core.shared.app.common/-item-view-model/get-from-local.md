//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[ItemViewModel](index.md)/[getFromLocal](get-from-local.md)

# getFromLocal

[common]\
open fun [getFromLocal](get-from-local.md)(idArg: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)? = null)
