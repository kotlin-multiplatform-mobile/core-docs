//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[ItemViewModel](index.md)/[getUseCase](get-use-case.md)

# getUseCase

[common]\
abstract val [getUseCase](get-use-case.md): [GET](index.md)
