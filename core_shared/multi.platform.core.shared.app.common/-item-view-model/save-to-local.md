//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[ItemViewModel](index.md)/[saveToLocal](save-to-local.md)

# saveToLocal

[common]\
open suspend fun [saveToLocal](save-to-local.md)(newItem: [ITEM](index.md)?)
