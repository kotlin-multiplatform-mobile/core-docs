//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[ItemViewModel](index.md)/[readUseCase](read-use-case.md)

# readUseCase

[common]\
abstract val [readUseCase](read-use-case.md): [READ](index.md)
