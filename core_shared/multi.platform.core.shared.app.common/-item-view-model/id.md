//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[ItemViewModel](index.md)/[id](id.md)

# id

[common]\
var [id](id.md): [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)?
