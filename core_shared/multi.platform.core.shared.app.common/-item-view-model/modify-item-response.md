//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[ItemViewModel](index.md)/[modifyItemResponse](modify-item-response.md)

# modifyItemResponse

[common]\
open fun [modifyItemResponse](modify-item-response.md)(item: [ITEM](index.md)): [ITEM](index.md)
