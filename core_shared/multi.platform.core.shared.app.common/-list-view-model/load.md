//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[ListViewModel](index.md)/[load](load.md)

# load

[common]\
open fun [load](load.md)(isFromNetworkOpt: [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) = isFromNetwork)
