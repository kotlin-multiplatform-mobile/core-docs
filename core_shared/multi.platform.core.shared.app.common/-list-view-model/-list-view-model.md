//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[ListViewModel](index.md)/[ListViewModel](-list-view-model.md)

# ListViewModel

[common]\
constructor(getUseCase: [GET](index.md), readUseCase: [READ](index.md), setUseCase: [SET](index.md))
