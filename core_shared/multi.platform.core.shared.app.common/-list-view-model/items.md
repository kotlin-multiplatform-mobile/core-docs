//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[ListViewModel](index.md)/[items](items.md)

# items

[common]\
var [items](items.md): MutableStateFlow&lt;[MutableList](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-mutable-list/index.html)&lt;[ITEM](index.md)&gt;?&gt;
