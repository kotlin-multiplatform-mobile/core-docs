//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[ListViewModel](index.md)/[saveToLocal](save-to-local.md)

# saveToLocal

[common]\
open suspend fun [saveToLocal](save-to-local.md)(newItems: [MutableList](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-mutable-list/index.html)&lt;[ITEM](index.md)&gt;?)
