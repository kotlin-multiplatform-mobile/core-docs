//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[ListViewModel](index.md)/[page](page.md)

# page

[common]\
var [page](page.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)
