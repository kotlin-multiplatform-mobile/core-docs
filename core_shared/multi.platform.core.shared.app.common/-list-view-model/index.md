//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[ListViewModel](index.md)

# ListViewModel

[common]\
abstract class [ListViewModel](index.md)&lt;[ITEM](index.md), [RESP](index.md), [GET](index.md) : [CoreUseCase](../../multi.platform.core.shared.domain.common.usecase/-core-use-case/index.md), [READ](index.md) : [CoreUseCase](../../multi.platform.core.shared.domain.common.usecase/-core-use-case/index.md), [SET](index.md) : [CoreUseCase](../../multi.platform.core.shared.domain.common.usecase/-core-use-case/index.md)&gt;(getUseCase: [GET](index.md), readUseCase: [READ](index.md), setUseCase: [SET](index.md)) : [CoreViewModel](../-core-view-model/index.md)

## Constructors

| | |
|---|---|
| [ListViewModel](-list-view-model.md) | [common]<br>constructor(getUseCase: [GET](index.md), readUseCase: [READ](index.md), setUseCase: [SET](index.md)) |

## Properties

| Name | Summary |
|---|---|
| [accessToken](../-core-view-model/access-token.md) | [common]<br>expect var [accessToken](../-core-view-model/access-token.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [errorConfirm](../-core-view-model/error-confirm.md) | [common]<br>expect var [errorConfirm](../-core-view-model/error-confirm.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [errorEmailFormat](../-core-view-model/error-email-format.md) | [common]<br>expect var [errorEmailFormat](../-core-view-model/error-email-format.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [errorEmptyField](../-core-view-model/error-empty-field.md) | [common]<br>expect var [errorEmptyField](../-core-view-model/error-empty-field.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [errorMessage](../-core-view-model/error-message.md) | [common]<br>expect val [errorMessage](../-core-view-model/error-message.md): MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt; |
| [errorMinChar](../-core-view-model/error-min-char.md) | [common]<br>expect var [errorMinChar](../-core-view-model/error-min-char.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [errorPasswordFormat](../-core-view-model/error-password-format.md) | [common]<br>expect var [errorPasswordFormat](../-core-view-model/error-password-format.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [errorPhoneFormat](../-core-view-model/error-phone-format.md) | [common]<br>expect var [errorPhoneFormat](../-core-view-model/error-phone-format.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [errorResponse](../-core-view-model/error-response.md) | [common]<br>expect val [errorResponse](../-core-view-model/error-response.md): MutableStateFlow&lt;HttpResponse?&gt; |
| [forceSignout](../-core-view-model/force-signout.md) | [common]<br>expect val [forceSignout](../-core-view-model/force-signout.md): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)&gt; |
| [isEmpty](../-core-view-model/is-empty.md) | [common]<br>expect val [isEmpty](../-core-view-model/is-empty.md): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)&gt; |
| [isFromNetwork](../-core-view-model/is-from-network.md) | [common]<br>expect var [isFromNetwork](../-core-view-model/is-from-network.md): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [items](items.md) | [common]<br>var [items](items.md): MutableStateFlow&lt;[MutableList](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-mutable-list/index.html)&lt;[ITEM](index.md)&gt;?&gt; |
| [limit](limit.md) | [common]<br>var [limit](limit.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [loadingIndicator](../-core-view-model/loading-indicator.md) | [common]<br>expect val [loadingIndicator](../-core-view-model/loading-indicator.md): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?&gt; |
| [onException](../-core-view-model/on-exception.md) | [common]<br>expect val [onException](../-core-view-model/on-exception.md): MutableStateFlow&lt;[Exception](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-exception/index.html)?&gt; |
| [order](order.md) | [common]<br>var [order](order.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [page](page.md) | [common]<br>var [page](page.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [scope](../-core-view-model/scope.md) | [common]<br>expect val [scope](../-core-view-model/scope.md): CoroutineScope |
| [search](search.md) | [common]<br>var [search](search.md): [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)? |
| [successMessage](../-core-view-model/success-message.md) | [common]<br>expect val [successMessage](../-core-view-model/success-message.md): MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt; |
| [toastMessage](../-core-view-model/toast-message.md) | [common]<br>expect val [toastMessage](../-core-view-model/toast-message.md): MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt; |
| [useAsyncNetworkCall](../-core-view-model/use-async-network-call.md) | [common]<br>expect var [useAsyncNetworkCall](../-core-view-model/use-async-network-call.md): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |

## Functions

| Name | Summary |
|---|---|
| [getFromLocal](get-from-local.md) | [common]<br>open fun [getFromLocal](get-from-local.md)() |
| [getFromNetwork](get-from-network.md) | [common]<br>open fun [getFromNetwork](get-from-network.md)() |
| [load](load.md) | [common]<br>open fun [load](load.md)(isFromNetworkOpt: [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) = isFromNetwork) |
| [modifyItemResponse](modify-item-response.md) | [common]<br>open fun [modifyItemResponse](modify-item-response.md)(key: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), item: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)?): [ITEM](index.md) |
| [saveToLocal](save-to-local.md) | [common]<br>open suspend fun [saveToLocal](save-to-local.md)(newItems: [MutableList](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-mutable-list/index.html)&lt;[ITEM](index.md)&gt;?) |
| [validateBlank](../-core-view-model/validate-blank.md) | [common]<br>expect fun [validateBlank](../-core-view-model/validate-blank.md)(field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [validateConfirm](../-core-view-model/validate-confirm.md) | [common]<br>expect fun [validateConfirm](../-core-view-model/validate-confirm.md)(field1: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, field2: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [validateEmailFormat](../-core-view-model/validate-email-format.md) | [common]<br>expect fun [validateEmailFormat](../-core-view-model/validate-email-format.md)(field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [validateMinChar](../-core-view-model/validate-min-char.md) | [common]<br>expect fun [validateMinChar](../-core-view-model/validate-min-char.md)(min: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [validatePasswordFormat](../-core-view-model/validate-password-format.md) | [common]<br>expect fun [validatePasswordFormat](../-core-view-model/validate-password-format.md)(field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [validatePhoneFormat](../-core-view-model/validate-phone-format.md) | [common]<br>expect fun [validatePhoneFormat](../-core-view-model/validate-phone-format.md)(field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
