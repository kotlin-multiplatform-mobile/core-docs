//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[ListViewModel](index.md)/[modifyItemResponse](modify-item-response.md)

# modifyItemResponse

[common]\
open fun [modifyItemResponse](modify-item-response.md)(key: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), item: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)?): [ITEM](index.md)
