//[core_shared](../../../index.md)/[multi.platform.core.shared.app.common](../index.md)/[ListViewModel](index.md)/[order](order.md)

# order

[common]\
var [order](order.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
