//[core_shared](../../index.md)/[multi.platform.core.shared.external](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [CoreConfig](-core-config/index.md) | [common]<br>interface [CoreConfig](-core-config/index.md) |
