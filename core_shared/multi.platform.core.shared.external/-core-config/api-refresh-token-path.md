//[core_shared](../../../index.md)/[multi.platform.core.shared.external](../index.md)/[CoreConfig](index.md)/[apiRefreshTokenPath](api-refresh-token-path.md)

# apiRefreshTokenPath

[common]\
abstract val [apiRefreshTokenPath](api-refresh-token-path.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
