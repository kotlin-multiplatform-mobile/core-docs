//[core_shared](../../../index.md)/[multi.platform.core.shared.external](../index.md)/[CoreConfig](index.md)

# CoreConfig

[common]\
interface [CoreConfig](index.md)

## Properties

| Name | Summary |
|---|---|
| [apiAuthPath](api-auth-path.md) | [common]<br>abstract val [apiAuthPath](api-auth-path.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [apiChannel](api-channel.md) | [common]<br>abstract val [apiChannel](api-channel.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [apiRefreshTokenPath](api-refresh-token-path.md) | [common]<br>abstract val [apiRefreshTokenPath](api-refresh-token-path.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [headerChannel](header-channel.md) | [common]<br>abstract val [headerChannel](header-channel.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [headerDeviceId](header-device-id.md) | [common]<br>abstract val [headerDeviceId](header-device-id.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [headerLanguage](header-language.md) | [common]<br>abstract val [headerLanguage](header-language.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [headerOs](header-os.md) | [common]<br>abstract val [headerOs](header-os.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [headerVersion](header-version.md) | [common]<br>abstract val [headerVersion](header-version.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
