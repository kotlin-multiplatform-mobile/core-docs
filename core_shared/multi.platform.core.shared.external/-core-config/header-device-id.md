//[core_shared](../../../index.md)/[multi.platform.core.shared.external](../index.md)/[CoreConfig](index.md)/[headerDeviceId](header-device-id.md)

# headerDeviceId

[common]\
abstract val [headerDeviceId](header-device-id.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
