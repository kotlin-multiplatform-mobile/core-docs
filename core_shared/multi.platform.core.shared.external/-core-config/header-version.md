//[core_shared](../../../index.md)/[multi.platform.core.shared.external](../index.md)/[CoreConfig](index.md)/[headerVersion](header-version.md)

# headerVersion

[common]\
abstract val [headerVersion](header-version.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
