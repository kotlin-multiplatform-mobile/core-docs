//[core_shared](../../../index.md)/[multi.platform.core.shared.external](../index.md)/[CoreConfig](index.md)/[apiChannel](api-channel.md)

# apiChannel

[common]\
abstract val [apiChannel](api-channel.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
