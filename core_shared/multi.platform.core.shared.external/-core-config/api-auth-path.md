//[core_shared](../../../index.md)/[multi.platform.core.shared.external](../index.md)/[CoreConfig](index.md)/[apiAuthPath](api-auth-path.md)

# apiAuthPath

[common]\
abstract val [apiAuthPath](api-auth-path.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
