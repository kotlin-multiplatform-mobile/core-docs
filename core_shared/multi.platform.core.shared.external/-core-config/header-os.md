//[core_shared](../../../index.md)/[multi.platform.core.shared.external](../index.md)/[CoreConfig](index.md)/[headerOs](header-os.md)

# headerOs

[common]\
abstract val [headerOs](header-os.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
