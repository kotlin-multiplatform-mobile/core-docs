//[core_shared](../../index.md)/[multi.platform.core.shared](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [AndroidPlatform](-android-platform/index.md) | [android]<br>class [AndroidPlatform](-android-platform/index.md) : [Platform](-platform/index.md) |
| [Context](-context/index.md) | [common, android, ios]<br>[common]<br>expect class [Context](-context/index.md)<br>[android]<br>actual typealias [Context](-context/index.md) = [Application](https://developer.android.com/reference/kotlin/android/app/Application.html)<br>[ios]<br>actual typealias [Context](-context/index.md) = NSObject |
| [CoreApplication](-core-application/index.md) | [android]<br>abstract class [CoreApplication](-core-application/index.md) : [Application](https://developer.android.com/reference/kotlin/android/app/Application.html) |
| [DecimalFormat](-decimal-format/index.md) | [common, android, ios]<br>[common]<br>expect class [DecimalFormat](-decimal-format/index.md)<br>[android, ios]<br>actual class [DecimalFormat](-decimal-format/index.md) |
| [IOSPlatform](-i-o-s-platform/index.md) | [ios]<br>class [IOSPlatform](-i-o-s-platform/index.md) : [Platform](-platform/index.md) |
| [Parcelable](-parcelable/index.md) | [common, android, ios]<br>[common]<br>expect interface [Parcelable](-parcelable/index.md)<br>[android]<br>actual interface [Parcelable](-parcelable/index.md) : [Parcelable](https://developer.android.com/reference/kotlin/android/os/Parcelable.html)<br>[ios]<br>actual interface [Parcelable](-parcelable/index.md) |
| [Parcelize](-parcelize/index.md) | [common, android]<br>[common]<br>@[Target](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.annotation/-target/index.html)(allowedTargets = [[AnnotationTarget.CLASS](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.annotation/-annotation-target/-c-l-a-s-s/index.html)])<br>expect annotation class [Parcelize](-parcelize/index.md)<br>[android]<br>actual typealias [Parcelize](-parcelize/index.md) = kotlinx.android.parcel.Parcelize |
| [Platform](-platform/index.md) | [common]<br>interface [Platform](-platform/index.md) |

## Functions

| Name | Summary |
|---|---|
| [createJson](create-json.md) | [common]<br>fun [createJson](create-json.md)(): Json |
| [formatDate](format-date.md) | [common, android, ios]<br>[common]<br>expect fun [formatDate](format-date.md)(dateString: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), fromFormat: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), toFormat: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)<br>[android, ios]<br>actual fun [formatDate](format-date.md)(dateString: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), fromFormat: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), toFormat: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [getLanguage](get-language.md) | [common]<br>expect fun [getLanguage](get-language.md)(context: [Context](-context/index.md)?): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [getPlatform](get-platform.md) | [common, android, ios]<br>[common]<br>expect fun [getPlatform](get-platform.md)(): [Platform](-platform/index.md)<br>[android, ios]<br>actual fun [getPlatform](get-platform.md)(): [Platform](-platform/index.md) |
| [getStringPref](get-string-pref.md) | [common]<br>expect fun [getStringPref](get-string-pref.md)(context: [Context](-context/index.md)?, name: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), key: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), default: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [platformModule](platform-module.md) | [common, android, ios]<br>[common]<br>expect fun [platformModule](platform-module.md)(): Module<br>[android, ios]<br>actual fun [platformModule](platform-module.md)(): Module |
| [putStringPref](put-string-pref.md) | [common]<br>expect fun [putStringPref](put-string-pref.md)(context: [Context](-context/index.md)?, name: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), key: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), value: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?) |
| [removePref](remove-pref.md) | [common]<br>expect fun [removePref](remove-pref.md)(context: [Context](-context/index.md)?, name: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), key: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)) |
