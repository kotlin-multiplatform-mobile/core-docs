//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[Context](index.md)

# Context

[common]\
expect class [Context](index.md)

[android]\
actual typealias [Context](index.md) = [Application](https://developer.android.com/reference/kotlin/android/app/Application.html)

[ios]\
actual typealias [Context](index.md) = NSObject
