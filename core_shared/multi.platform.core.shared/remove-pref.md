//[core_shared](../../index.md)/[multi.platform.core.shared](index.md)/[removePref](remove-pref.md)

# removePref

[common]\
expect fun [removePref](remove-pref.md)(context: [Context](-context/index.md)?, name: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), key: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html))
