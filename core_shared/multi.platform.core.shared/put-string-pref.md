//[core_shared](../../index.md)/[multi.platform.core.shared](index.md)/[putStringPref](put-string-pref.md)

# putStringPref

[common]\
expect fun [putStringPref](put-string-pref.md)(context: [Context](-context/index.md)?, name: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), key: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), value: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?)
