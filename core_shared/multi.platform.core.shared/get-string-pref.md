//[core_shared](../../index.md)/[multi.platform.core.shared](index.md)/[getStringPref](get-string-pref.md)

# getStringPref

[common]\
expect fun [getStringPref](get-string-pref.md)(context: [Context](-context/index.md)?, name: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), key: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), default: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
