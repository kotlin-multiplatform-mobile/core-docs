//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[AndroidPlatform](index.md)

# AndroidPlatform

[android]\
class [AndroidPlatform](index.md) : [Platform](../-platform/index.md)

## Constructors

| | |
|---|---|
| [AndroidPlatform](-android-platform.md) | [android]<br>constructor() |

## Properties

| Name | Summary |
|---|---|
| [apiEngine](api-engine.md) | [android]<br>open override val [apiEngine](api-engine.md): HttpClientEngine |
| [name](name.md) | [android]<br>open override val [name](name.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
