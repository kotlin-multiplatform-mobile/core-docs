//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[AndroidPlatform](index.md)/[name](name.md)

# name

[android]\
open override val [name](name.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
