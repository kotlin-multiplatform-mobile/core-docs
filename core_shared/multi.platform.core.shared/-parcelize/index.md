//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[Parcelize](index.md)

# Parcelize

[common]\
@[Target](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.annotation/-target/index.html)(allowedTargets = [[AnnotationTarget.CLASS](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.annotation/-annotation-target/-c-l-a-s-s/index.html)])

expect annotation class [Parcelize](index.md)

[android]\
actual typealias [Parcelize](index.md) = kotlinx.android.parcel.Parcelize
