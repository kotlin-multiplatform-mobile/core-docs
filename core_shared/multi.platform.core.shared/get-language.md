//[core_shared](../../index.md)/[multi.platform.core.shared](index.md)/[getLanguage](get-language.md)

# getLanguage

[common]\
expect fun [getLanguage](get-language.md)(context: [Context](-context/index.md)?): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
