//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[Parcelable](index.md)

# Parcelable

[common]\
expect interface [Parcelable](index.md)

[android]\
actual interface [Parcelable](index.md) : [Parcelable](https://developer.android.com/reference/kotlin/android/os/Parcelable.html)

[ios]\
actual interface [Parcelable](index.md)

## Functions

| Name | Summary |
|---|---|
| [describeContents](index.md#-1578325224%2FFunctions%2F-2121679934) | [android]<br>abstract fun [describeContents](index.md#-1578325224%2FFunctions%2F-2121679934)(): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [writeToParcel](index.md#-1754457655%2FFunctions%2F-2121679934) | [android]<br>abstract fun [writeToParcel](index.md#-1754457655%2FFunctions%2F-2121679934)(p0: [Parcel](https://developer.android.com/reference/kotlin/android/os/Parcel.html), p1: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)) |
