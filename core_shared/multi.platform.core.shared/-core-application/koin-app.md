//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[CoreApplication](index.md)/[koinApp](koin-app.md)

# koinApp

[android]\
abstract val [koinApp](koin-app.md): KoinAppDeclaration

Abstract variable must override KOIN App to add any modules
