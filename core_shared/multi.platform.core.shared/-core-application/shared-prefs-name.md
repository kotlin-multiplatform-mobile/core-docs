//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[CoreApplication](index.md)/[sharedPrefsName](shared-prefs-name.md)

# sharedPrefsName

[android]\
abstract fun [sharedPrefsName](shared-prefs-name.md)(): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

Abstract function must override shared preferences name
