//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[CoreApplication](index.md)/[appVersion](app-version.md)

# appVersion

[android]\
abstract fun [appVersion](app-version.md)(): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

Abstract function must override app version
