//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[CoreApplication](index.md)/[protocol](protocol.md)

# protocol

[android]\
open fun [protocol](protocol.md)(): URLProtocol

Open function to override server protocol
