//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[CoreApplication](index.md)/[provideX509TrustManager](provide-x509-trust-manager.md)

# provideX509TrustManager

[android]\
open fun [provideX509TrustManager](provide-x509-trust-manager.md)(): [X509TrustManager](https://developer.android.com/reference/kotlin/javax/net/ssl/X509TrustManager.html)

Open function for trust manager
