//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[CoreApplication](index.md)/[provideOkHttpClient](provide-ok-http-client.md)

# provideOkHttpClient

[android]\
open fun [provideOkHttpClient](provide-ok-http-client.md)(x509TrustManager: [X509TrustManager](https://developer.android.com/reference/kotlin/javax/net/ssl/X509TrustManager.html)): OkHttpClient.Builder

Open function for okhttp client builder
