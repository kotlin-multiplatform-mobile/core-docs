//[core_shared](../../../../index.md)/[multi.platform.core.shared](../../index.md)/[CoreApplication](../index.md)/[NetworkClientType](index.md)

# NetworkClientType

[android]\
enum [NetworkClientType](index.md) : [Enum](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-enum/index.html)&lt;[CoreApplication.NetworkClientType](index.md)&gt;

## Entries

| | |
|---|---|
| [KTOR](-k-t-o-r/index.md) | [android]<br>[KTOR](-k-t-o-r/index.md) |
| [RETROFIT](-r-e-t-r-o-f-i-t/index.md) | [android]<br>[RETROFIT](-r-e-t-r-o-f-i-t/index.md) |

## Properties

| Name | Summary |
|---|---|
| [entries](entries.md) | [android]<br>val [entries](entries.md): [EnumEntries](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.enums/-enum-entries/index.html)&lt;[CoreApplication.NetworkClientType](index.md)&gt;<br>Returns a representation of an immutable list of all enum entries, in the order they're declared. |
| [name](-r-e-t-r-o-f-i-t/index.md#-372974862%2FProperties%2F-2121679934) | [android]<br>val [name](-r-e-t-r-o-f-i-t/index.md#-372974862%2FProperties%2F-2121679934): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [ordinal](-r-e-t-r-o-f-i-t/index.md#-739389684%2FProperties%2F-2121679934) | [android]<br>val [ordinal](-r-e-t-r-o-f-i-t/index.md#-739389684%2FProperties%2F-2121679934): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |

## Functions

| Name | Summary |
|---|---|
| [valueOf](value-of.md) | [android]<br>fun [valueOf](value-of.md)(value: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [CoreApplication.NetworkClientType](index.md)<br>Returns the enum constant of this type with the specified name. The string must match exactly an identifier used to declare an enum constant in this type. (Extraneous whitespace characters are not permitted.) |
| [values](values.md) | [android]<br>fun [values](values.md)(): [Array](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-array/index.html)&lt;[CoreApplication.NetworkClientType](index.md)&gt;<br>Returns an array containing the constants of this enum type, in the order they're declared. |
