//[core_shared](../../../../index.md)/[multi.platform.core.shared](../../index.md)/[CoreApplication](../index.md)/[NetworkClientType](index.md)/[entries](entries.md)

# entries

[android]\
val [entries](entries.md): [EnumEntries](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.enums/-enum-entries/index.html)&lt;[CoreApplication.NetworkClientType](index.md)&gt;

Returns a representation of an immutable list of all enum entries, in the order they're declared.

This method may be used to iterate over the enum entries.
