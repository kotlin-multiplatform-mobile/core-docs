//[core_shared](../../../../../index.md)/[multi.platform.core.shared](../../../index.md)/[CoreApplication](../../index.md)/[NetworkClientType](../index.md)/[KTOR](index.md)

# KTOR

[android]\
[KTOR](index.md)

## Properties

| Name | Summary |
|---|---|
| [name](../-r-e-t-r-o-f-i-t/index.md#-372974862%2FProperties%2F-2121679934) | [android]<br>val [name](../-r-e-t-r-o-f-i-t/index.md#-372974862%2FProperties%2F-2121679934): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [ordinal](../-r-e-t-r-o-f-i-t/index.md#-739389684%2FProperties%2F-2121679934) | [android]<br>val [ordinal](../-r-e-t-r-o-f-i-t/index.md#-739389684%2FProperties%2F-2121679934): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
