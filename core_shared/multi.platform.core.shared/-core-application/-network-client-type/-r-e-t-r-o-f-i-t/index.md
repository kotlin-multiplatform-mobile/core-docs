//[core_shared](../../../../../index.md)/[multi.platform.core.shared](../../../index.md)/[CoreApplication](../../index.md)/[NetworkClientType](../index.md)/[RETROFIT](index.md)

# RETROFIT

[android]\
[RETROFIT](index.md)

## Properties

| Name | Summary |
|---|---|
| [name](index.md#-372974862%2FProperties%2F-2121679934) | [android]<br>val [name](index.md#-372974862%2FProperties%2F-2121679934): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [ordinal](index.md#-739389684%2FProperties%2F-2121679934) | [android]<br>val [ordinal](index.md#-739389684%2FProperties%2F-2121679934): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
