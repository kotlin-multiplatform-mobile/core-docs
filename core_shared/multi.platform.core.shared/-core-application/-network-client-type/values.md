//[core_shared](../../../../index.md)/[multi.platform.core.shared](../../index.md)/[CoreApplication](../index.md)/[NetworkClientType](index.md)/[values](values.md)

# values

[android]\
fun [values](values.md)(): [Array](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-array/index.html)&lt;[CoreApplication.NetworkClientType](index.md)&gt;

Returns an array containing the constants of this enum type, in the order they're declared.

This method may be used to iterate over the constants.
