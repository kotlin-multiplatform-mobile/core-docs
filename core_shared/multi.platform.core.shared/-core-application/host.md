//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[CoreApplication](index.md)/[host](host.md)

# host

[android]\
abstract fun [host](host.md)(): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

Abstract function must override server host
