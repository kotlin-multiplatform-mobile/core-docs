//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[CoreApplication](index.md)/[deviceId](device-id.md)

# deviceId

[android]\
abstract fun [deviceId](device-id.md)(): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

Abstract function must override device id
