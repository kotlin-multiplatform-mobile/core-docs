//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[CoreApplication](index.md)/[networkClientType](network-client-type.md)

# networkClientType

[android]\
open fun [networkClientType](network-client-type.md)(): [CoreApplication.NetworkClientType](-network-client-type/index.md)

Open function for network client used Default: NetworkClientType.KTOR
