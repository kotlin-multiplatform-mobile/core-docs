//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[CoreApplication](index.md)/[onConfigurationChanged](on-configuration-changed.md)

# onConfigurationChanged

[android]\
open override fun [onConfigurationChanged](on-configuration-changed.md)(newConfig: [Configuration](https://developer.android.com/reference/kotlin/android/content/res/Configuration.html))
