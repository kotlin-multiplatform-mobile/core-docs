//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[Platform](index.md)/[apiEngine](api-engine.md)

# apiEngine

[common]\
abstract val [apiEngine](api-engine.md): HttpClientEngine
