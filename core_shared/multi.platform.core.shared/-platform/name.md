//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[Platform](index.md)/[name](name.md)

# name

[common]\
abstract val [name](name.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
