//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[Platform](index.md)

# Platform

interface [Platform](index.md)

#### Inheritors

| |
|---|
| AndroidPlatform |
| IOSPlatform |

## Properties

| Name | Summary |
|---|---|
| [apiEngine](api-engine.md) | [common]<br>abstract val [apiEngine](api-engine.md): HttpClientEngine |
| [name](name.md) | [common]<br>abstract val [name](name.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
