//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[IOSPlatform](index.md)/[name](name.md)

# name

[ios]\
open override val [name](name.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
