//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[IOSPlatform](index.md)

# IOSPlatform

[ios]\
class [IOSPlatform](index.md) : [Platform](../-platform/index.md)

## Constructors

| | |
|---|---|
| [IOSPlatform](-i-o-s-platform.md) | [ios]<br>constructor() |

## Properties

| Name | Summary |
|---|---|
| [apiEngine](api-engine.md) | [ios]<br>open override val [apiEngine](api-engine.md): HttpClientEngine |
| [name](name.md) | [ios]<br>open override val [name](name.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
