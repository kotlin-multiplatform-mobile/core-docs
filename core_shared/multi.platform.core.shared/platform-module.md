//[core_shared](../../index.md)/[multi.platform.core.shared](index.md)/[platformModule](platform-module.md)

# platformModule

[common, android, ios]\
[common]\
expect fun [platformModule](platform-module.md)(): Module

[android, ios]\
actual fun [platformModule](platform-module.md)(): Module
