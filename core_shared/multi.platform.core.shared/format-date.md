//[core_shared](../../index.md)/[multi.platform.core.shared](index.md)/[formatDate](format-date.md)

# formatDate

[common, android, ios]\
[common]\
expect fun [formatDate](format-date.md)(dateString: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), fromFormat: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), toFormat: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

[android, ios]\
actual fun [formatDate](format-date.md)(dateString: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), fromFormat: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), toFormat: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
