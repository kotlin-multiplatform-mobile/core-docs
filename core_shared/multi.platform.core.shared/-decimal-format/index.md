//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[DecimalFormat](index.md)

# DecimalFormat

[common]\
expect class [DecimalFormat](index.md)

[android, ios]\
actual class [DecimalFormat](index.md)

## Constructors

| | |
|---|---|
| [DecimalFormat](-decimal-format.md) | [android, ios]<br>constructor()<br>[common]<br>expect constructor() |

## Functions

| Name | Summary |
|---|---|
| [format](format.md) | [common, android, ios]<br>[common]<br>expect fun [format](format.md)(double: [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html), maximumFractionDigits: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)<br>[android, ios]<br>actual fun [format](format.md)(double: [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html), maximumFractionDigits: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
