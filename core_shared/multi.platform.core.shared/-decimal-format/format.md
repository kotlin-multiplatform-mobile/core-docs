//[core_shared](../../../index.md)/[multi.platform.core.shared](../index.md)/[DecimalFormat](index.md)/[format](format.md)

# format

[common, android, ios]\
[common]\
expect fun [format](format.md)(double: [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html), maximumFractionDigits: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

[android, ios]\
actual fun [format](format.md)(double: [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html), maximumFractionDigits: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
