//[core_shared](../../index.md)/[multi.platform.core.shared](index.md)/[getPlatform](get-platform.md)

# getPlatform

[common, android, ios]\
[common]\
expect fun [getPlatform](get-platform.md)(): [Platform](-platform/index.md)

[android, ios]\
actual fun [getPlatform](get-platform.md)(): [Platform](-platform/index.md)
