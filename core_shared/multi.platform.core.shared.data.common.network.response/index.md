//[core_shared](../../index.md)/[multi.platform.core.shared.data.common.network.response](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [CommonResponse](-common-response/index.md) | [common]<br>@Serializable<br>data class [CommonResponse](-common-response/index.md)&lt;[D](-common-response/index.md)&gt;(var data: [D](-common-response/index.md)? = null, var results: [D](-common-response/index.md)? = null) |
