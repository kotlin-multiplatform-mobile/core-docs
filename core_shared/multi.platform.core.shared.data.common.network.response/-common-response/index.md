//[core_shared](../../../index.md)/[multi.platform.core.shared.data.common.network.response](../index.md)/[CommonResponse](index.md)

# CommonResponse

[common]\
@Serializable

data class [CommonResponse](index.md)&lt;[D](index.md)&gt;(var data: [D](index.md)? = null, var results: [D](index.md)? = null)

## Constructors

| | |
|---|---|
| [CommonResponse](-common-response.md) | [common]<br>constructor(data: [D](index.md)? = null, results: [D](index.md)? = null) |

## Properties

| Name | Summary |
|---|---|
| [data](data.md) | [common]<br>var [data](data.md): [D](index.md)? |
| [results](results.md) | [common]<br>var [results](results.md): [D](index.md)? |
