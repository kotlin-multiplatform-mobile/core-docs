//[core_shared](../../../index.md)/[multi.platform.core.shared.data.common.network.response](../index.md)/[CommonResponse](index.md)/[CommonResponse](-common-response.md)

# CommonResponse

[common]\
constructor(data: [D](index.md)? = null, results: [D](index.md)? = null)
