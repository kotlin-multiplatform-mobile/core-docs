//[core_shared](../../index.md)/[multi.platform.core.shared.external.enums](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [ErrorEnum](-error-enum/index.md) | [common]<br>enum [ErrorEnum](-error-enum/index.md) : [Enum](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-enum/index.html)&lt;[ErrorEnum](-error-enum/index.md)&gt; |
| [StateEnum](-state-enum/index.md) | [common]<br>enum [StateEnum](-state-enum/index.md) : [Enum](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-enum/index.html)&lt;[StateEnum](-state-enum/index.md)&gt; |
