//[core_shared](../../../../index.md)/[multi.platform.core.shared.external.enums](../../index.md)/[ErrorEnum](../index.md)/[AuthError](index.md)

# AuthError

[common]\
[AuthError](index.md)

## Properties

| Name | Summary |
|---|---|
| [name](../../-state-enum/-e-m-p-t-y/index.md#-372974862%2FProperties%2F-1689394408) | [common]<br>val [name](../../-state-enum/-e-m-p-t-y/index.md#-372974862%2FProperties%2F-1689394408): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [ordinal](../../-state-enum/-e-m-p-t-y/index.md#-739389684%2FProperties%2F-1689394408) | [common]<br>val [ordinal](../../-state-enum/-e-m-p-t-y/index.md#-739389684%2FProperties%2F-1689394408): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
