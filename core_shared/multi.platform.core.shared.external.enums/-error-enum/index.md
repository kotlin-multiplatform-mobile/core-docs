//[core_shared](../../../index.md)/[multi.platform.core.shared.external.enums](../index.md)/[ErrorEnum](index.md)

# ErrorEnum

[common]\
enum [ErrorEnum](index.md) : [Enum](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-enum/index.html)&lt;[ErrorEnum](index.md)&gt;

## Entries

| | |
|---|---|
| [AccountLocked](-account-locked/index.md) | [common]<br>[AccountLocked](-account-locked/index.md) |
| [AuthError](-auth-error/index.md) | [common]<br>[AuthError](-auth-error/index.md) |
| [DataNotFound](-data-not-found/index.md) | [common]<br>[DataNotFound](-data-not-found/index.md) |
| [TokenError](-token-error/index.md) | [common]<br>[TokenError](-token-error/index.md) |
| [TooManyRetryOTPLogin](-too-many-retry-o-t-p-login/index.md) | [common]<br>[TooManyRetryOTPLogin](-too-many-retry-o-t-p-login/index.md) |
| [TooManyRetryOTPRegistration](-too-many-retry-o-t-p-registration/index.md) | [common]<br>[TooManyRetryOTPRegistration](-too-many-retry-o-t-p-registration/index.md) |
| [TooManyRetryEmailVerification](-too-many-retry-email-verification/index.md) | [common]<br>[TooManyRetryEmailVerification](-too-many-retry-email-verification/index.md) |
| [TooManyChangeEmailVerification](-too-many-change-email-verification/index.md) | [common]<br>[TooManyChangeEmailVerification](-too-many-change-email-verification/index.md) |
| [RequestOTPErrorLogin](-request-o-t-p-error-login/index.md) | [common]<br>[RequestOTPErrorLogin](-request-o-t-p-error-login/index.md) |
| [RequestOTPErrorRegistration](-request-o-t-p-error-registration/index.md) | [common]<br>[RequestOTPErrorRegistration](-request-o-t-p-error-registration/index.md) |

## Properties

| Name | Summary |
|---|---|
| [entries](entries.md) | [common]<br>val [entries](entries.md): [EnumEntries](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.enums/-enum-entries/index.html)&lt;[ErrorEnum](index.md)&gt;<br>Returns a representation of an immutable list of all enum entries, in the order they're declared. |
| [name](../-state-enum/-e-m-p-t-y/index.md#-372974862%2FProperties%2F-1689394408) | [common]<br>val [name](../-state-enum/-e-m-p-t-y/index.md#-372974862%2FProperties%2F-1689394408): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [ordinal](../-state-enum/-e-m-p-t-y/index.md#-739389684%2FProperties%2F-1689394408) | [common]<br>val [ordinal](../-state-enum/-e-m-p-t-y/index.md#-739389684%2FProperties%2F-1689394408): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |

## Functions

| Name | Summary |
|---|---|
| [valueOf](value-of.md) | [common]<br>fun [valueOf](value-of.md)(value: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [ErrorEnum](index.md)<br>Returns the enum constant of this type with the specified name. The string must match exactly an identifier used to declare an enum constant in this type. (Extraneous whitespace characters are not permitted.) |
| [values](values.md) | [common]<br>fun [values](values.md)(): [Array](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-array/index.html)&lt;[ErrorEnum](index.md)&gt;<br>Returns an array containing the constants of this enum type, in the order they're declared. |
