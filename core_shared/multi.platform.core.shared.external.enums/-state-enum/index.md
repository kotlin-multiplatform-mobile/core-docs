//[core_shared](../../../index.md)/[multi.platform.core.shared.external.enums](../index.md)/[StateEnum](index.md)

# StateEnum

[common]\
enum [StateEnum](index.md) : [Enum](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-enum/index.html)&lt;[StateEnum](index.md)&gt;

## Entries

| | |
|---|---|
| [LOADING](-l-o-a-d-i-n-g/index.md) | [common]<br>[LOADING](-l-o-a-d-i-n-g/index.md) |
| [SUCCESS](-s-u-c-c-e-s-s/index.md) | [common]<br>[SUCCESS](-s-u-c-c-e-s-s/index.md) |
| [ERROR](-e-r-r-o-r/index.md) | [common]<br>[ERROR](-e-r-r-o-r/index.md) |
| [EMPTY](-e-m-p-t-y/index.md) | [common]<br>[EMPTY](-e-m-p-t-y/index.md) |

## Properties

| Name | Summary |
|---|---|
| [entries](entries.md) | [common]<br>val [entries](entries.md): [EnumEntries](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.enums/-enum-entries/index.html)&lt;[StateEnum](index.md)&gt;<br>Returns a representation of an immutable list of all enum entries, in the order they're declared. |
| [name](-e-m-p-t-y/index.md#-372974862%2FProperties%2F-1689394408) | [common]<br>val [name](-e-m-p-t-y/index.md#-372974862%2FProperties%2F-1689394408): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [ordinal](-e-m-p-t-y/index.md#-739389684%2FProperties%2F-1689394408) | [common]<br>val [ordinal](-e-m-p-t-y/index.md#-739389684%2FProperties%2F-1689394408): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |

## Functions

| Name | Summary |
|---|---|
| [valueOf](value-of.md) | [common]<br>fun [valueOf](value-of.md)(value: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [StateEnum](index.md)<br>Returns the enum constant of this type with the specified name. The string must match exactly an identifier used to declare an enum constant in this type. (Extraneous whitespace characters are not permitted.) |
| [values](values.md) | [common]<br>fun [values](values.md)(): [Array](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-array/index.html)&lt;[StateEnum](index.md)&gt;<br>Returns an array containing the constants of this enum type, in the order they're declared. |
