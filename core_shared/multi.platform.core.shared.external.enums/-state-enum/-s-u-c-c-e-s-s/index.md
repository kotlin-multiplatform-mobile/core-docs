//[core_shared](../../../../index.md)/[multi.platform.core.shared.external.enums](../../index.md)/[StateEnum](../index.md)/[SUCCESS](index.md)

# SUCCESS

[common]\
[SUCCESS](index.md)

## Properties

| Name | Summary |
|---|---|
| [name](../-e-m-p-t-y/index.md#-372974862%2FProperties%2F-1689394408) | [common]<br>val [name](../-e-m-p-t-y/index.md#-372974862%2FProperties%2F-1689394408): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [ordinal](../-e-m-p-t-y/index.md#-739389684%2FProperties%2F-1689394408) | [common]<br>val [ordinal](../-e-m-p-t-y/index.md#-739389684%2FProperties%2F-1689394408): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
