//[core_shared](../../../../index.md)/[multi.platform.core.shared.external.enums](../../index.md)/[StateEnum](../index.md)/[EMPTY](index.md)

# EMPTY

[common]\
[EMPTY](index.md)

## Properties

| Name | Summary |
|---|---|
| [name](index.md#-372974862%2FProperties%2F-1689394408) | [common]<br>val [name](index.md#-372974862%2FProperties%2F-1689394408): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [ordinal](index.md#-739389684%2FProperties%2F-1689394408) | [common]<br>val [ordinal](index.md#-739389684%2FProperties%2F-1689394408): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
