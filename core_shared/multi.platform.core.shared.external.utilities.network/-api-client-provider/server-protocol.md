//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities.network](../index.md)/[ApiClientProvider](index.md)/[serverProtocol](server-protocol.md)

# serverProtocol

[common]\
abstract val [serverProtocol](server-protocol.md): URLProtocol?
