//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities.network](../index.md)/[ApiClientProvider](index.md)/[deviceId](device-id.md)

# deviceId

[common]\
abstract val [deviceId](device-id.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
