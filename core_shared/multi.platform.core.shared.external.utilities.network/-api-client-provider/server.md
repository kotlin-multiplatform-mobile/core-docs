//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities.network](../index.md)/[ApiClientProvider](index.md)/[server](server.md)

# server

[common]\
abstract val [server](server.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
