//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities.network](../index.md)/[ApiClientProvider](index.md)/[client](client.md)

# client

[common]\
abstract val [client](client.md): [T](index.md)
