//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities.network](../index.md)/[ApiClientProvider](index.md)/[version](version.md)

# version

[common]\
abstract val [version](version.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
