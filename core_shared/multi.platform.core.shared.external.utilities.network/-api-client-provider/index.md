//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities.network](../index.md)/[ApiClientProvider](index.md)

# ApiClientProvider

interface [ApiClientProvider](index.md)&lt;[T](index.md)&gt;

#### Inheritors

| |
|---|
| RetrofitApiClientImpl |
| [KtorApiClientImpl](../-ktor-api-client-impl/index.md) |

## Properties

| Name | Summary |
|---|---|
| [client](client.md) | [common]<br>abstract val [client](client.md): [T](index.md) |
| [context](context.md) | [common]<br>abstract val [context](context.md): [Context](../../multi.platform.core.shared/-context/index.md)? |
| [coreConfig](core-config.md) | [common]<br>abstract val [coreConfig](core-config.md): [CoreConfig](../../multi.platform.core.shared.external/-core-config/index.md) |
| [deviceId](device-id.md) | [common]<br>abstract val [deviceId](device-id.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [json](json.md) | [common]<br>abstract val [json](json.md): Json |
| [refreshTokenUseCase](refresh-token-use-case.md) | [common]<br>abstract val [refreshTokenUseCase](refresh-token-use-case.md): [RefreshTokenUseCase](../../multi.platform.core.shared.domain.common.usecase/-refresh-token-use-case/index.md)? |
| [server](server.md) | [common]<br>abstract val [server](server.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [serverProtocol](server-protocol.md) | [common]<br>abstract val [serverProtocol](server-protocol.md): URLProtocol? |
| [sharedPrefsKey](shared-prefs-key.md) | [common]<br>abstract val [sharedPrefsKey](shared-prefs-key.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [version](version.md) | [common]<br>abstract val [version](version.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
