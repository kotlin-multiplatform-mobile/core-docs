//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities.network](../index.md)/[ApiClientProvider](index.md)/[refreshTokenUseCase](refresh-token-use-case.md)

# refreshTokenUseCase

[common]\
abstract val [refreshTokenUseCase](refresh-token-use-case.md): [RefreshTokenUseCase](../../multi.platform.core.shared.domain.common.usecase/-refresh-token-use-case/index.md)?
