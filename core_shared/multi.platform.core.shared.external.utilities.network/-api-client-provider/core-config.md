//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities.network](../index.md)/[ApiClientProvider](index.md)/[coreConfig](core-config.md)

# coreConfig

[common]\
abstract val [coreConfig](core-config.md): [CoreConfig](../../multi.platform.core.shared.external/-core-config/index.md)
