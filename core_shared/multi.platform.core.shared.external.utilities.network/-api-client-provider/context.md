//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities.network](../index.md)/[ApiClientProvider](index.md)/[context](context.md)

# context

[common]\
abstract val [context](context.md): [Context](../../multi.platform.core.shared/-context/index.md)?
