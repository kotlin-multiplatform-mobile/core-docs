//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities.network](../index.md)/[KtorApiClientImpl](index.md)/[deviceId](device-id.md)

# deviceId

[common]\
open override val [deviceId](device-id.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
