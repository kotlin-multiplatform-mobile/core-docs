//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities.network](../index.md)/[KtorApiClientImpl](index.md)

# KtorApiClientImpl

[common]\
class [KtorApiClientImpl](index.md)(httpClientEngine: HttpClientEngine, val context: [Context](../../multi.platform.core.shared/-context/index.md)?, val refreshTokenUseCase: [RefreshTokenUseCase](../../multi.platform.core.shared.domain.common.usecase/-refresh-token-use-case/index.md)?, val json: Json, val coreConfig: [CoreConfig](../../multi.platform.core.shared.external/-core-config/index.md), val server: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), val serverProtocol: URLProtocol?, val sharedPrefsKey: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), val deviceId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), val version: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)) : [ApiClientProvider](../-api-client-provider/index.md)&lt;HttpClient&gt;

## Constructors

| | |
|---|---|
| [KtorApiClientImpl](-ktor-api-client-impl.md) | [common]<br>constructor(httpClientEngine: HttpClientEngine, context: [Context](../../multi.platform.core.shared/-context/index.md)?, refreshTokenUseCase: [RefreshTokenUseCase](../../multi.platform.core.shared.domain.common.usecase/-refresh-token-use-case/index.md)?, json: Json, coreConfig: [CoreConfig](../../multi.platform.core.shared.external/-core-config/index.md), server: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), serverProtocol: URLProtocol?, sharedPrefsKey: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), deviceId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), version: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)) |

## Properties

| Name | Summary |
|---|---|
| [client](client.md) | [common]<br>open override val [client](client.md): HttpClient |
| [context](context.md) | [common]<br>open override val [context](context.md): [Context](../../multi.platform.core.shared/-context/index.md)? |
| [coreConfig](core-config.md) | [common]<br>open override val [coreConfig](core-config.md): [CoreConfig](../../multi.platform.core.shared.external/-core-config/index.md) |
| [deviceId](device-id.md) | [common]<br>open override val [deviceId](device-id.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [json](json.md) | [common]<br>open override val [json](json.md): Json |
| [refreshTokenUseCase](refresh-token-use-case.md) | [common]<br>open override val [refreshTokenUseCase](refresh-token-use-case.md): [RefreshTokenUseCase](../../multi.platform.core.shared.domain.common.usecase/-refresh-token-use-case/index.md)? |
| [server](server.md) | [common]<br>open override val [server](server.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [serverProtocol](server-protocol.md) | [common]<br>open override val [serverProtocol](server-protocol.md): URLProtocol? |
| [sharedPrefsKey](shared-prefs-key.md) | [common]<br>open override val [sharedPrefsKey](shared-prefs-key.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [version](version.md) | [common]<br>open override val [version](version.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
