//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities.network](../index.md)/[KtorApiClientImpl](index.md)/[version](version.md)

# version

[common]\
open override val [version](version.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
