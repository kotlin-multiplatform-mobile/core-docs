//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities.network](../index.md)/[RetrofitApiClientImpl](index.md)/[version](version.md)

# version

[android]\
open override val [version](version.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
