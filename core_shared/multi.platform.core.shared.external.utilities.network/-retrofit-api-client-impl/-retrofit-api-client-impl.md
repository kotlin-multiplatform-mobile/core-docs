//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities.network](../index.md)/[RetrofitApiClientImpl](index.md)/[RetrofitApiClientImpl](-retrofit-api-client-impl.md)

# RetrofitApiClientImpl

[android]\
constructor(okHttpClientBuilder: OkHttpClient.Builder, context: [Context](../../multi.platform.core.shared/-context/index.md)?, refreshTokenUseCase: [RefreshTokenUseCase](../../multi.platform.core.shared.domain.common.usecase/-refresh-token-use-case/index.md)?, json: Json, coreConfig: [CoreConfig](../../multi.platform.core.shared.external/-core-config/index.md), server: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), sharedPrefsKey: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), deviceId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), version: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), serverProtocol: URLProtocol? = null)
