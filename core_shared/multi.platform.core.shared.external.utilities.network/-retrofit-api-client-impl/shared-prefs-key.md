//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities.network](../index.md)/[RetrofitApiClientImpl](index.md)/[sharedPrefsKey](shared-prefs-key.md)

# sharedPrefsKey

[android]\
open override val [sharedPrefsKey](shared-prefs-key.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
