//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities.network](../index.md)/[RetrofitApiClientImpl](index.md)/[context](context.md)

# context

[android]\
open override val [context](context.md): [Context](../../multi.platform.core.shared/-context/index.md)?
