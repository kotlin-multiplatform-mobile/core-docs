//[core_shared](../../../index.md)/[multi.platform.core.shared.external.utilities.network](../index.md)/[RetrofitApiClientImpl](index.md)

# RetrofitApiClientImpl

[android]\
class [RetrofitApiClientImpl](index.md)(okHttpClientBuilder: OkHttpClient.Builder, val context: [Context](../../multi.platform.core.shared/-context/index.md)?, val refreshTokenUseCase: [RefreshTokenUseCase](../../multi.platform.core.shared.domain.common.usecase/-refresh-token-use-case/index.md)?, val json: Json, val coreConfig: [CoreConfig](../../multi.platform.core.shared.external/-core-config/index.md), val server: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), val sharedPrefsKey: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), val deviceId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), val version: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), val serverProtocol: URLProtocol? = null) : [ApiClientProvider](../-api-client-provider/index.md)&lt;Retrofit&gt;

## Constructors

| | |
|---|---|
| [RetrofitApiClientImpl](-retrofit-api-client-impl.md) | [android]<br>constructor(okHttpClientBuilder: OkHttpClient.Builder, context: [Context](../../multi.platform.core.shared/-context/index.md)?, refreshTokenUseCase: [RefreshTokenUseCase](../../multi.platform.core.shared.domain.common.usecase/-refresh-token-use-case/index.md)?, json: Json, coreConfig: [CoreConfig](../../multi.platform.core.shared.external/-core-config/index.md), server: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), sharedPrefsKey: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), deviceId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), version: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), serverProtocol: URLProtocol? = null) |

## Properties

| Name | Summary |
|---|---|
| [client](client.md) | [android]<br>open override val [client](client.md): Retrofit |
| [context](context.md) | [android]<br>open override val [context](context.md): [Context](../../multi.platform.core.shared/-context/index.md)? |
| [coreConfig](core-config.md) | [android]<br>open override val [coreConfig](core-config.md): [CoreConfig](../../multi.platform.core.shared.external/-core-config/index.md) |
| [deviceId](device-id.md) | [android]<br>open override val [deviceId](device-id.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [json](json.md) | [android]<br>open override val [json](json.md): Json |
| [refreshTokenUseCase](refresh-token-use-case.md) | [android]<br>open override val [refreshTokenUseCase](refresh-token-use-case.md): [RefreshTokenUseCase](../../multi.platform.core.shared.domain.common.usecase/-refresh-token-use-case/index.md)? |
| [server](server.md) | [android]<br>open override val [server](server.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [serverProtocol](server-protocol.md) | [android]<br>open override val [serverProtocol](server-protocol.md): URLProtocol? = null |
| [sharedPrefsKey](shared-prefs-key.md) | [android]<br>open override val [sharedPrefsKey](shared-prefs-key.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [version](version.md) | [android]<br>open override val [version](version.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
