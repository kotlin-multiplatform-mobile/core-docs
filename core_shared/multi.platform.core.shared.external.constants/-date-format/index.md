//[core_shared](../../../index.md)/[multi.platform.core.shared.external.constants](../index.md)/[DateFormat](index.md)

# DateFormat

[common]\
object [DateFormat](index.md)

## Properties

| Name | Summary |
|---|---|
| [API_DATE_FORMAT](-a-p-i_-d-a-t-e_-f-o-r-m-a-t.md) | [common]<br>const val [API_DATE_FORMAT](-a-p-i_-d-a-t-e_-f-o-r-m-a-t.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [NORMAL_DATE_FORMAT](-n-o-r-m-a-l_-d-a-t-e_-f-o-r-m-a-t.md) | [common]<br>const val [NORMAL_DATE_FORMAT](-n-o-r-m-a-l_-d-a-t-e_-f-o-r-m-a-t.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
