//[core_shared](../../../index.md)/[multi.platform.core.shared.external.constants](../index.md)/[DateFormat](index.md)/[NORMAL_DATE_FORMAT](-n-o-r-m-a-l_-d-a-t-e_-f-o-r-m-a-t.md)

# NORMAL_DATE_FORMAT

[common]\
const val [NORMAL_DATE_FORMAT](-n-o-r-m-a-l_-d-a-t-e_-f-o-r-m-a-t.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
