//[core_shared](../../../index.md)/[multi.platform.core.shared.external.constants](../index.md)/[DateFormat](index.md)/[API_DATE_FORMAT](-a-p-i_-d-a-t-e_-f-o-r-m-a-t.md)

# API_DATE_FORMAT

[common]\
const val [API_DATE_FORMAT](-a-p-i_-d-a-t-e_-f-o-r-m-a-t.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
