//[core_shared](../../../index.md)/[multi.platform.core.shared.external.constants](../index.md)/[CommonKey](index.md)/[FILTERED_KEY](-f-i-l-t-e-r-e-d_-k-e-y.md)

# FILTERED_KEY

[common]\
const val [FILTERED_KEY](-f-i-l-t-e-r-e-d_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
