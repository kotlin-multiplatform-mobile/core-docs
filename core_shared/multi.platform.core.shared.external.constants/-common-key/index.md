//[core_shared](../../../index.md)/[multi.platform.core.shared.external.constants](../index.md)/[CommonKey](index.md)

# CommonKey

[common]\
object [CommonKey](index.md)

## Properties

| Name | Summary |
|---|---|
| [ACCESS_TOKEN_KEY](-a-c-c-e-s-s_-t-o-k-e-n_-k-e-y.md) | [common]<br>const val [ACCESS_TOKEN_KEY](-a-c-c-e-s-s_-t-o-k-e-n_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [FILTERED_KEY](-f-i-l-t-e-r-e-d_-k-e-y.md) | [common]<br>const val [FILTERED_KEY](-f-i-l-t-e-r-e-d_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [ONBOARDING_KEY](-o-n-b-o-a-r-d-i-n-g_-k-e-y.md) | [common]<br>const val [ONBOARDING_KEY](-o-n-b-o-a-r-d-i-n-g_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [PAYMENT_KEY](-p-a-y-m-e-n-t_-k-e-y.md) | [common]<br>const val [PAYMENT_KEY](-p-a-y-m-e-n-t_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [PHONE_KEY](-p-h-o-n-e_-k-e-y.md) | [common]<br>const val [PHONE_KEY](-p-h-o-n-e_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [REFRESH_TOKEN_KEY](-r-e-f-r-e-s-h_-t-o-k-e-n_-k-e-y.md) | [common]<br>const val [REFRESH_TOKEN_KEY](-r-e-f-r-e-s-h_-t-o-k-e-n_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [RETRY_KEY](-r-e-t-r-y_-k-e-y.md) | [common]<br>const val [RETRY_KEY](-r-e-t-r-y_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [SELECT_DATE_KEY](-s-e-l-e-c-t_-d-a-t-e_-k-e-y.md) | [common]<br>const val [SELECT_DATE_KEY](-s-e-l-e-c-t_-d-a-t-e_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
