//[core_shared](../../../index.md)/[multi.platform.core.shared.external.constants](../index.md)/[CommonKey](index.md)/[ONBOARDING_KEY](-o-n-b-o-a-r-d-i-n-g_-k-e-y.md)

# ONBOARDING_KEY

[common]\
const val [ONBOARDING_KEY](-o-n-b-o-a-r-d-i-n-g_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
