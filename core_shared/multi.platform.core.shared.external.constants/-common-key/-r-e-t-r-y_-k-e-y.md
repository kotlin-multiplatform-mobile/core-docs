//[core_shared](../../../index.md)/[multi.platform.core.shared.external.constants](../index.md)/[CommonKey](index.md)/[RETRY_KEY](-r-e-t-r-y_-k-e-y.md)

# RETRY_KEY

[common]\
const val [RETRY_KEY](-r-e-t-r-y_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
