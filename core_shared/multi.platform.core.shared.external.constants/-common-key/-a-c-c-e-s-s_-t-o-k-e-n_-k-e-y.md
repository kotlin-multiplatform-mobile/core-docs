//[core_shared](../../../index.md)/[multi.platform.core.shared.external.constants](../index.md)/[CommonKey](index.md)/[ACCESS_TOKEN_KEY](-a-c-c-e-s-s_-t-o-k-e-n_-k-e-y.md)

# ACCESS_TOKEN_KEY

[common]\
const val [ACCESS_TOKEN_KEY](-a-c-c-e-s-s_-t-o-k-e-n_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
