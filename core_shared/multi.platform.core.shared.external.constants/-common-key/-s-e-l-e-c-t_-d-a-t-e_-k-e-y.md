//[core_shared](../../../index.md)/[multi.platform.core.shared.external.constants](../index.md)/[CommonKey](index.md)/[SELECT_DATE_KEY](-s-e-l-e-c-t_-d-a-t-e_-k-e-y.md)

# SELECT_DATE_KEY

[common]\
const val [SELECT_DATE_KEY](-s-e-l-e-c-t_-d-a-t-e_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
