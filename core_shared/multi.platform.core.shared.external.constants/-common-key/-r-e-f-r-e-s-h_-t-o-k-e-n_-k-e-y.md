//[core_shared](../../../index.md)/[multi.platform.core.shared.external.constants](../index.md)/[CommonKey](index.md)/[REFRESH_TOKEN_KEY](-r-e-f-r-e-s-h_-t-o-k-e-n_-k-e-y.md)

# REFRESH_TOKEN_KEY

[common]\
const val [REFRESH_TOKEN_KEY](-r-e-f-r-e-s-h_-t-o-k-e-n_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
