//[core_shared](../../../index.md)/[multi.platform.core.shared.external.constants](../index.md)/[CommonKey](index.md)/[PAYMENT_KEY](-p-a-y-m-e-n-t_-k-e-y.md)

# PAYMENT_KEY

[common]\
const val [PAYMENT_KEY](-p-a-y-m-e-n-t_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
