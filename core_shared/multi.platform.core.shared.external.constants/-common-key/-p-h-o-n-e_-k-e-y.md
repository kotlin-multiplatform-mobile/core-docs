//[core_shared](../../../index.md)/[multi.platform.core.shared.external.constants](../index.md)/[CommonKey](index.md)/[PHONE_KEY](-p-h-o-n-e_-k-e-y.md)

# PHONE_KEY

[common]\
const val [PHONE_KEY](-p-h-o-n-e_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
