//[core_shared](../../index.md)/[multi.platform.core.shared.external.constants](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [CommonKey](-common-key/index.md) | [common]<br>object [CommonKey](-common-key/index.md) |
| [DateFormat](-date-format/index.md) | [common]<br>object [DateFormat](-date-format/index.md) |
