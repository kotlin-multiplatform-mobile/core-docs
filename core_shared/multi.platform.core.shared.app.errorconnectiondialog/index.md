//[core_shared](../../index.md)/[multi.platform.core.shared.app.errorconnectiondialog](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [ErrorConnectionDialogFragment](-error-connection-dialog-fragment/index.md) | [android]<br>class [ErrorConnectionDialogFragment](-error-connection-dialog-fragment/index.md) : [CoreDialogFragment](../multi.platform.core.shared.app.common/-core-dialog-fragment/index.md) |
