//[core_shared](../../../index.md)/[multi.platform.core.shared.app.errorconnectiondialog](../index.md)/[ErrorConnectionDialogFragment](index.md)/[isCancelable](is-cancelable.md)

# isCancelable

[android]\
open override fun [isCancelable](is-cancelable.md)(): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)
