//[core_shared](../../../index.md)/[multi.platform.core.shared.app.errorconnectiondialog](../index.md)/[ErrorConnectionDialogFragment](index.md)/[onViewCreated](on-view-created.md)

# onViewCreated

[android]\
open override fun [onViewCreated](on-view-created.md)(view: [View](https://developer.android.com/reference/kotlin/android/view/View.html), savedInstanceState: [Bundle](https://developer.android.com/reference/kotlin/android/os/Bundle.html)?)
